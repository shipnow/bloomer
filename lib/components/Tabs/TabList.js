"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
var isAlignOption = helpers_1.isOption(bulma_1.isLeft, bulma_1.isCenter, bulma_1.isRight);
function TabList(_a) {
    var _b;
    var _c = _a.tag, tag = _c === void 0 ? 'ul' : _c, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames(tslib_1.__assign({}, (isAlignOption(props.isAlign) ? (_b = {}, _b["is-" + props.isAlign] = true, _b) : {})), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.TabList = TabList;
var HOC = bulma_1.withHelpersModifiers(TabList);
exports.default = HOC;
//# sourceMappingURL=TabList.js.map