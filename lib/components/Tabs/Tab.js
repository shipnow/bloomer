"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Tab(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'li' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames(tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tab = Tab;
var HOC = bulma_1.withHelpersModifiers(Tab);
exports.default = HOC;
//# sourceMappingURL=Tab.js.map