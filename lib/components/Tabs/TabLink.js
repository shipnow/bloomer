"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var bulma_1 = require("./../../bulma");
function TabLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    if (render)
        return render(tslib_1.__assign({}, props));
    return React.createElement(tag, props);
}
exports.TabLink = TabLink;
var HOC = bulma_1.withHelpersModifiers(TabLink);
exports.default = HOC;
//# sourceMappingURL=TabLink.js.map