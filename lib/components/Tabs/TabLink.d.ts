import * as React from 'react';
import { Bulma } from './../../bulma';
export interface TabLink<T> extends Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
}
export declare function TabLink({ tag, render, ...props }: TabLink<HTMLElement>): any;
declare const HOC: React.StatelessComponent<TabLink<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
