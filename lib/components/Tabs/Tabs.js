"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Tabs(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('tabs', tslib_1.__assign({ 'is-boxed': props.isBoxed, 'is-toggle': props.isToggle }, helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var isBoxed = props.isBoxed, isToggle = props.isToggle, rest = tslib_1.__rest(props, ["isBoxed", "isToggle"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tabs = Tabs;
var HOC = bulma_1.withHelpersModifiers(Tabs);
exports.default = HOC;
//# sourceMappingURL=Tabs.js.map