import * as React from 'react';
import { Bulma } from './../../bulma';
export interface MenuLink<T> extends Bulma.Tag, Bulma.Render, Bulma.Active, React.HTMLProps<T> {
}
export declare function MenuLink({ tag, render, ...props }: MenuLink<HTMLElement>): any;
declare const HOC: React.StatelessComponent<MenuLink<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
