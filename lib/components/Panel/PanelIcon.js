"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var bulma_1 = require("./../../bulma");
function PanelIcon(_a) {
    var children = _a.children, props = tslib_1.__rest(_a, ["children"]);
    var icon = (React.createElement("span", tslib_1.__assign({}, props, { className: "panel-icon" }),
        React.createElement("i", { className: "" + props.className, "aria-hidden": "true" })));
    return icon;
}
exports.PanelIcon = PanelIcon;
var HOC = bulma_1.withHelpersModifiers(PanelIcon);
exports.default = HOC;
//# sourceMappingURL=PanelIcon.js.map