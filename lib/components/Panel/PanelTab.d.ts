import * as React from 'react';
import { Bulma } from './../../bulma';
export interface PanelTab<T> extends Bulma.Render, Bulma.Tag, Bulma.Active, React.HTMLProps<T> {
}
export declare function PanelTab({ tag, render, ...props }: PanelTab<HTMLElement>): any;
declare const HOC: React.StatelessComponent<PanelTab<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
