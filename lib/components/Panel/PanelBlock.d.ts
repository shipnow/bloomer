import * as React from 'react';
import { Bulma } from './../../bulma';
export interface PanelBlock<T> extends Bulma.Render, Bulma.Tag, Bulma.Active, React.HTMLProps<T> {
    isWrapped?: boolean;
}
export declare function PanelBlock({ tag, render, ...props }: PanelBlock<HTMLElement>): any;
declare const HOC: React.StatelessComponent<PanelBlock<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
