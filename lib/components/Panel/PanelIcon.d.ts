import * as React from 'react';
import { Bulma } from './../../bulma';
export interface PanelIcon<T> extends React.HTMLProps<T> {
}
export declare function PanelIcon({ children, ...props }: PanelIcon<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<PanelIcon<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
