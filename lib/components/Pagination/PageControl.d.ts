import * as React from 'react';
import { Bulma } from './../../bulma';
export interface PageControl<T> extends Bulma.Render, Bulma.Tag, Bulma.Active, Bulma.Focused, React.HTMLProps<T> {
    isPrevious?: boolean;
    isNext?: boolean;
}
export declare function PageControl({ tag, render, ...props }: PageControl<HTMLElement>): any;
declare const HOC: React.StatelessComponent<PageControl<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
