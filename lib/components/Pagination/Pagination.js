"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Pagination(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('pagination', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Pagination = Pagination;
var HOC = bulma_1.withHelpersModifiers(Pagination);
exports.default = HOC;
//# sourceMappingURL=Pagination.js.map