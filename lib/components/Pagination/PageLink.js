"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function PageLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('pagination-link', tslib_1.__assign({ 'is-current': props.isCurrent }, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getFocusedModifiers)), props.className);
    var isCurrent = props.isCurrent, rest = tslib_1.__rest(props, ["isCurrent"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers, bulma_1.removeFocusedModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.PageLink = PageLink;
var HOC = bulma_1.withHelpersModifiers(PageLink);
exports.default = HOC;
//# sourceMappingURL=PageLink.js.map