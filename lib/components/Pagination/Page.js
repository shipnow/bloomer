"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var bulma_1 = require("./../../bulma");
function Page(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'li' : _b, props = tslib_1.__rest(_a, ["tag"]);
    return React.createElement(tag, props);
}
exports.Page = Page;
var HOC = bulma_1.withHelpersModifiers(Page);
exports.default = HOC;
//# sourceMappingURL=Page.js.map