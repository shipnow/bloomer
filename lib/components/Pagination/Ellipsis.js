"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Ellipsis(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'span' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('pagination-ellipsis', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getFocusedModifiers)), props.className);
    var _c = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers, bulma_1.removeFocusedModifiers), children = _c.children, HTMLProps = tslib_1.__rest(_c, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }), '\u2026');
}
exports.Ellipsis = Ellipsis;
var HOC = bulma_1.withHelpersModifiers(Ellipsis);
exports.default = HOC;
//# sourceMappingURL=Ellipsis.js.map