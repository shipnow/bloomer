import * as React from 'react';
import { Bulma } from './../../bulma';
export interface PageLink<T> extends Bulma.Render, Bulma.Tag, Bulma.Active, Bulma.Focused, React.HTMLProps<T> {
    isCurrent?: boolean;
}
export declare function PageLink({ tag, render, ...props }: PageLink<HTMLElement>): any;
declare const HOC: React.StatelessComponent<PageLink<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
