"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function MediaLeft(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('media-left', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MediaLeft = MediaLeft;
var HOC = bulma_1.withHelpersModifiers(MediaLeft);
exports.default = HOC;
//# sourceMappingURL=MediaLeft.js.map