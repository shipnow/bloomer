"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var classNames = require("classnames");
var bulma_1 = require("./../../../bulma");
function CardHeaderTitle(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-header-title', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardHeaderTitle = CardHeaderTitle;
var HOC = bulma_1.withHelpersModifiers(CardHeaderTitle);
exports.default = HOC;
//# sourceMappingURL=CardHeaderTitle.js.map