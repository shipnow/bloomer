"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var React = require("react");
var classNames = require("classnames");
var bulma_1 = require("./../../../bulma");
function CardHeaderIcon(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('card-header-icon', props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardHeaderIcon = CardHeaderIcon;
var HOC = bulma_1.withHelpersModifiers(CardHeaderIcon);
exports.default = HOC;
//# sourceMappingURL=CardHeaderIcon.js.map