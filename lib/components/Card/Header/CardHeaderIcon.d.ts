import * as React from 'react';
import { Bulma } from './../../../bulma';
export interface CardHeaderIcon<T> extends Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
}
export declare function CardHeaderIcon({ tag, render, ...props }: CardHeaderIcon<HTMLElement>): any;
declare const HOC: React.StatelessComponent<CardHeaderIcon<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
