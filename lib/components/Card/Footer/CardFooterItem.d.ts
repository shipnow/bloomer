import * as React from 'react';
import { Bulma } from './../../../bulma';
export interface CardFooterItem<T> extends Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
}
export declare function CardFooterItem({ tag, render, ...props }: CardFooterItem<HTMLElement>): any;
declare const HOC: React.StatelessComponent<CardFooterItem<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
