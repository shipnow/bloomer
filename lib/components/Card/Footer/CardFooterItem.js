"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
function CardFooterItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('card-footer-item', props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, props, { className: className }));
}
exports.CardFooterItem = CardFooterItem;
var HOC = bulma_1.withHelpersModifiers(CardFooterItem);
exports.default = HOC;
//# sourceMappingURL=CardFooterItem.js.map