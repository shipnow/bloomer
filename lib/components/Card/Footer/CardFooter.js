"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
function CardFooter(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-footer', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardFooter = CardFooter;
var HOC = bulma_1.withHelpersModifiers(CardFooter);
exports.default = HOC;
//# sourceMappingURL=CardFooter.js.map