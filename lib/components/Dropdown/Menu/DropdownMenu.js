"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
function DropdownMenu(_a) {
    var props = tslib_1.__rest(_a, []);
    var className = classNames('dropdown-menu', props.className);
    return React.createElement('div', tslib_1.__assign({}, props, { className: className }));
}
exports.DropdownMenu = DropdownMenu;
var HOC = bulma_1.withHelpersModifiers(DropdownMenu);
exports.default = HOC;
//# sourceMappingURL=DropdownMenu.js.map