import * as React from 'react';
import { Bulma } from './../../../bulma';
export interface DropdownItem<T> extends Bulma.Active, Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
}
export declare function DropdownItem({ tag, render, ...props }: DropdownItem<HTMLElement>): any;
declare const HOC: React.StatelessComponent<DropdownItem<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
