"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Dropdown(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, isHoverable = _a.isHoverable, props = tslib_1.__rest(_a, ["tag", "isHoverable"]);
    var className = classNames('dropdown', tslib_1.__assign({ 'is-hoverable': isHoverable }, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getAlignmentModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers, bulma_1.removeAlignmentProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Dropdown = Dropdown;
var HOC = bulma_1.withHelpersModifiers(Dropdown);
exports.default = HOC;
//# sourceMappingURL=Dropdown.js.map