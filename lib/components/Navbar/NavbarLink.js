"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function NavbarLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('navbar-link', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavbarLink = NavbarLink;
var HOC = bulma_1.withHelpersModifiers(NavbarLink);
exports.default = HOC;
//# sourceMappingURL=NavbarLink.js.map