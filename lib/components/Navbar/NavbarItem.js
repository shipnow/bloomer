"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function NavbarItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, isHoverable = _a.isHoverable, hasDropdown = _a.hasDropdown, props = tslib_1.__rest(_a, ["tag", "render", "isHoverable", "hasDropdown"]);
    var className = classNames('navbar-item', tslib_1.__assign({ 'has-dropdown': hasDropdown, 'is-hoverable': isHoverable }, bulma_1.getActiveModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavbarItem = NavbarItem;
var HOC = bulma_1.withHelpersModifiers(NavbarItem);
exports.default = HOC;
//# sourceMappingURL=NavbarItem.js.map