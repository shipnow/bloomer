"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function NavbarBurger(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-burger', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var _c = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers), children = _c.children, HTMLProps = tslib_1.__rest(_c, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }), React.createElement('span', null), React.createElement('span', null), React.createElement('span', null));
}
exports.NavbarBurger = NavbarBurger;
var HOC = bulma_1.withHelpersModifiers(NavbarBurger);
exports.default = HOC;
//# sourceMappingURL=NavbarBurger.js.map