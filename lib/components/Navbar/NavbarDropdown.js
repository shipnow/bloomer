"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function NavbarDropdown(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, isBoxed = _a.isBoxed, props = tslib_1.__rest(_a, ["tag", "isBoxed"]);
    var className = classNames('navbar-dropdown', {
        'is-boxed': isBoxed,
    }, props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarDropdown = NavbarDropdown;
var HOC = bulma_1.withHelpersModifiers(NavbarDropdown);
exports.default = HOC;
//# sourceMappingURL=NavbarDropdown.js.map