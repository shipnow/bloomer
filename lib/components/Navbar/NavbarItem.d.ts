import * as React from 'react';
import { Bulma } from './../../bulma';
export interface NavbarItem<T> extends Bulma.Tag, Bulma.Render, Bulma.Active, React.HTMLProps<T> {
    isHoverable?: boolean;
    hasDropdown?: boolean;
}
export declare function NavbarItem({ tag, render, isHoverable, hasDropdown, ...props }: NavbarItem<HTMLElement>): any;
declare const HOC: React.StatelessComponent<NavbarItem<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
