"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function NavbarDivider(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'hr' : _b, isBoxed = _a.isBoxed, props = tslib_1.__rest(_a, ["tag", "isBoxed"]);
    var className = classNames('navbar-divider', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarDivider = NavbarDivider;
var HOC = bulma_1.withHelpersModifiers(NavbarDivider);
exports.default = HOC;
//# sourceMappingURL=NavbarDivider.js.map