"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function Navbar(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, isTransparent = _a.isTransparent, props = tslib_1.__rest(_a, ["tag", "isTransparent"]);
    var className = classNames('navbar', {
        'is-transparent': isTransparent,
    }, props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Navbar = Navbar;
var HOC = bulma_1.withHelpersModifiers(Navbar);
exports.default = HOC;
//# sourceMappingURL=Navbar.js.map