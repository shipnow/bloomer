import * as React from 'react';
import { Bulma } from './../../bulma';
export interface NavbarLink<T> extends Bulma.Render, Bulma.Tag, Bulma.Active, React.HTMLProps<T> {
}
export declare function NavbarLink({ tag, render, ...props }: NavbarLink<HTMLElement>): any;
declare const HOC: React.StatelessComponent<NavbarLink<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
