import * as React from 'react';
import { Bulma } from './../../bulma';
export interface ModalClose<T> extends Bulma.Size, Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
}
export declare function ModalClose({ tag, render, ...props }: ModalClose<HTMLElement>): any;
declare const HOC: React.StatelessComponent<ModalClose<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
