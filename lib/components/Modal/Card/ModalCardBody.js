"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
function ModalCardBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'section' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCardBody = ModalCardBody;
var HOC = bulma_1.withHelpersModifiers(ModalCardBody);
exports.default = HOC;
//# sourceMappingURL=ModalCardBody.js.map