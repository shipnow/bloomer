"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function ModalClose(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'button' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('modal-close', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.ModalClose = ModalClose;
var HOC = bulma_1.withHelpersModifiers(ModalClose);
exports.default = HOC;
//# sourceMappingURL=ModalClose.js.map