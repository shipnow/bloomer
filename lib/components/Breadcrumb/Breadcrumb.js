"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
var separatorClassname = function (separator) {
    var _a;
    return separator ? (_a = {}, _a["has-" + separator + "-separator"] = true, _a) : {};
};
function Breadcrumb(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, hasSeparator = _a.hasSeparator, props = tslib_1.__rest(_a, ["tag", "hasSeparator"]);
    var className = classNames('breadcrumb', tslib_1.__assign({}, separatorClassname(hasSeparator), helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Breadcrumb = Breadcrumb;
var HOC = bulma_1.withHelpersModifiers(Breadcrumb);
exports.default = HOC;
//# sourceMappingURL=Breadcrumb.js.map