"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function LevelItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level-item', {
        'is-flexible': props.isFlexible,
    }, props.className);
    var isFlexible = props.isFlexible, HTMLProps = tslib_1.__rest(props, ["isFlexible"]);
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.LevelItem = LevelItem;
var HOC = bulma_1.withHelpersModifiers(LevelItem);
exports.default = HOC;
//# sourceMappingURL=LevelItem.js.map