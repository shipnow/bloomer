"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function Level(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level', {
        'is-mobile': props.isMobile,
    }, props.className);
    var isMobile = props.isMobile, HTMLProps = tslib_1.__rest(props, ["isMobile"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Level = Level;
var HOC = bulma_1.withHelpersModifiers(Level);
exports.default = HOC;
//# sourceMappingURL=Level.js.map