"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Message(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'article' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('message', tslib_1.__assign({}, bulma_1.getColorModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Message = Message;
var HOC = bulma_1.withHelpersModifiers(Message);
exports.default = HOC;
//# sourceMappingURL=Message.js.map