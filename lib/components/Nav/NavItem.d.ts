import * as React from 'react';
import { Bulma } from './../../bulma';
export interface NavItem<T> extends Bulma.Tag, Bulma.Render, Bulma.Active, React.HTMLProps<T> {
    isTab?: boolean;
    isBrand?: boolean;
}
export declare function NavItem({ tag, render, ...props }: NavItem<HTMLElement>): any;
declare const HOC: React.StatelessComponent<NavItem<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
