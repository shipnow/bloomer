"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function NavRight(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav-right', tslib_1.__assign({ 'nav-menu': props.isMenu }, bulma_1.getActiveModifiers(props)), props.className);
    var isMenu = props.isMenu, rest = tslib_1.__rest(props, ["isMenu"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavRight = NavRight;
var HOC = bulma_1.withHelpersModifiers(NavRight);
exports.default = HOC;
//# sourceMappingURL=NavRight.js.map