"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function Nav(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav', {
        'has-shadow': props.hasShadow,
    }, props.className);
    var hasShadow = props.hasShadow, HTMLProps = tslib_1.__rest(props, ["hasShadow"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Nav = Nav;
var HOC = bulma_1.withHelpersModifiers(Nav);
exports.default = HOC;
//# sourceMappingURL=Nav.js.map