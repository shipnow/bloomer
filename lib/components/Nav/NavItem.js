"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function NavItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('nav-item', tslib_1.__assign({ 'is-brand': props.isBrand, 'is-tab': props.isTab }, bulma_1.getActiveModifiers(props)), props.className);
    var isTab = props.isTab, isBrand = props.isBrand, rest = tslib_1.__rest(props, ["isTab", "isBrand"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavItem = NavItem;
var HOC = bulma_1.withHelpersModifiers(NavItem);
exports.default = HOC;
//# sourceMappingURL=NavItem.js.map