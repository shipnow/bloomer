"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Hero(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'section' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero', tslib_1.__assign({ 'is-bold': props.isBold, 'is-fullheight': props.isFullHeight, 'is-halfheight': props.isHalfHeight }, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers)), props.className);
    var isBold = props.isBold, isFullHeight = props.isFullHeight, rest = tslib_1.__rest(props, ["isBold", "isFullHeight"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeColorProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Hero = Hero;
var HOC = bulma_1.withHelpersModifiers(Hero);
exports.default = HOC;
//# sourceMappingURL=Hero.js.map