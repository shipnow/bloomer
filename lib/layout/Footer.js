"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
function Footer(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('footer', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Footer = Footer;
var HOC = bulma_1.withHelpersModifiers(Footer);
exports.default = HOC;
//# sourceMappingURL=Footer.js.map