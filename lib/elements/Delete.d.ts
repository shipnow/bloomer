import * as React from 'react';
import { Bulma } from './../bulma';
export interface Delete<T> extends Bulma.Render, Bulma.Size, React.HTMLProps<T> {
}
export declare function Delete(props: Delete<HTMLButtonElement | HTMLAnchorElement>): any;
declare const HOC: React.StatelessComponent<Delete<HTMLButtonElement | HTMLAnchorElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
