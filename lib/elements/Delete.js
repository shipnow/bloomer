"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
function Delete(props) {
    var className = classNames('delete', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var render = props.render, rest = tslib_1.__rest(props, ["render"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    var anchor = (React.createElement("a", tslib_1.__assign({ role: "button" }, HTMLProps, { className: className })));
    var button = (React.createElement("button", tslib_1.__assign({ type: props.type || 'button' }, HTMLProps, { className: className })));
    return props.href ? anchor : button;
}
exports.Delete = Delete;
var HOC = bulma_1.withHelpersModifiers(Delete);
exports.default = HOC;
//# sourceMappingURL=Delete.js.map