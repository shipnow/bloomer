import * as React from 'react';
import { Bulma } from './../bulma';
export interface Size {
    isSize?: '16x16' | '24x24' | '32x32' | '48x48' | '64x64' | '96x96' | '128x128';
}
export interface Ratio {
    isRatio?: 'square' | '1:1' | '4:3' | '3:2' | '16:9' | '2:1';
}
export interface Image<T> extends Size, Ratio, React.HTMLProps<T> {
}
export declare function Image(props: Image<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Image<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
