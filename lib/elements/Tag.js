"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
function Tag(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'span' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('tag', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tag = Tag;
var HOC = bulma_1.withHelpersModifiers(Tag);
exports.default = HOC;
//# sourceMappingURL=Tag.js.map