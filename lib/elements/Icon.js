"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
var isAlignOption = helpers_1.isOption(bulma_1.isLeft, bulma_1.isRight);
function Icon(_a) {
    var _b;
    var children = _a.children, props = tslib_1.__rest(_a, ["children"]);
    var className = classNames('icon', tslib_1.__assign({}, (isAlignOption(props.isAlign) ? (_b = {}, _b["is-" + props.isAlign] = true, _b) : {}), bulma_1.getSizeModifiers(props)));
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    var icon = (React.createElement("span", tslib_1.__assign({}, HTMLProps, { className: className }),
        React.createElement("i", { className: "" + props.className, "aria-hidden": "true" })));
    return icon;
}
exports.Icon = Icon;
var HOC = bulma_1.withHelpersModifiers(Icon);
exports.default = HOC;
//# sourceMappingURL=Icon.js.map