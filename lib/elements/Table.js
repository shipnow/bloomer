"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
function Table(props) {
    var className = classNames('table', {
        'is-bordered': props.isBordered,
        'is-narrow': props.isNarrow,
        'is-striped': props.isStriped,
    }, props.className);
    var isBordered = props.isBordered, isStriped = props.isStriped, isNarrow = props.isNarrow, HTMLProps = tslib_1.__rest(props, ["isBordered", "isStriped", "isNarrow"]);
    return (React.createElement("table", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.Table = Table;
var HOC = bulma_1.withHelpersModifiers(Table);
exports.default = HOC;
//# sourceMappingURL=Table.js.map