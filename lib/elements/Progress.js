"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
function Progress(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'progress' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('progress', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getSizeModifiers, bulma_1.getColorModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Progress = Progress;
var HOC = bulma_1.withHelpersModifiers(Progress);
exports.default = HOC;
//# sourceMappingURL=Progress.js.map