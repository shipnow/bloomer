import * as React from 'react';
import { Bulma } from './../bulma';
export interface Icon<T> extends Bulma.Size, Bulma.Alignment, React.HTMLProps<T> {
    isAlign?: 'left' | 'right';
}
export declare function Icon({ children, ...props }: Icon<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Icon<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
