"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
function Title(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'h1' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('title', tslib_1.__assign({}, bulma_1.getHeadingModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeHeadingProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Title = Title;
var HOC = bulma_1.withHelpersModifiers(Title);
exports.default = HOC;
//# sourceMappingURL=Title.js.map