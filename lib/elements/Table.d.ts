import * as React from 'react';
import { Bulma } from './../bulma';
export interface Table<T> extends React.HTMLProps<T> {
    isBordered?: boolean;
    isStriped?: boolean;
    isNarrow?: boolean;
}
export declare function Table(props: Table<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Table<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
