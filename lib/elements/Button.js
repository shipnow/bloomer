"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
function Button(props) {
    var className = classNames('button', tslib_1.__assign({ 'is-inverted': props.isInverted, 'is-link': props.isLink, 'is-outlined': props.isOutlined, 'is-static': props.isStatic }, helpers_1.combineModifiers(props, bulma_1.getStateModifiers, bulma_1.getColorModifiers, bulma_1.getLoadingModifiers, bulma_1.getSizeModifiers)), props.className);
    var render = props.render, isLink = props.isLink, isOutlined = props.isOutlined, isInverted = props.isInverted, isStatic = props.isStatic, rest = tslib_1.__rest(props, ["render", "isLink", "isOutlined", "isInverted", "isStatic"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeStateProps, bulma_1.removeColorProps, bulma_1.removeLoadingProps, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    var anchor = (React.createElement("a", tslib_1.__assign({}, HTMLProps, { role: "button", className: className })));
    var button = (React.createElement("button", tslib_1.__assign({}, HTMLProps, { type: props.type || 'button', className: className })));
    return props.href ? anchor : button;
}
exports.Button = Button;
var HOC = bulma_1.withHelpersModifiers(Button);
exports.default = HOC;
//# sourceMappingURL=Button.js.map