import * as React from 'react';
import { Bulma } from './../bulma';
export interface Button<T> extends Bulma.Render, Bulma.State, Bulma.Color, Bulma.Loading, Bulma.Size, React.HTMLProps<T> {
    isLink?: boolean;
    isOutlined?: boolean;
    isInverted?: boolean;
    isStatic?: boolean;
}
export declare function Button(props: Button<HTMLButtonElement | HTMLAnchorElement>): any;
declare const HOC: React.StatelessComponent<Button<HTMLButtonElement | HTMLAnchorElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
