import * as React from 'react';
import { Bulma } from './../../bulma';
export declare function Checkbox(props: React.HTMLProps<HTMLInputElement>): JSX.Element;
declare const HOC: React.StatelessComponent<React.HTMLProps<HTMLInputElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
