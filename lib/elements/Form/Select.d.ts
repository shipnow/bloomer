import * as React from 'react';
import { Bulma } from './../../bulma';
export interface Select<T> extends Bulma.Color, Bulma.Size, Bulma.Loading, React.HTMLProps<T> {
}
export declare function Select(props: Select<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Select<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
