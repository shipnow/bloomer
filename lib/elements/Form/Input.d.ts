import * as React from 'react';
import { Bulma } from './../../bulma';
export interface Input<T> extends Bulma.Color, Bulma.Size, Bulma.State, React.HTMLProps<T> {
}
export declare function Input(props: Input<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Input<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
