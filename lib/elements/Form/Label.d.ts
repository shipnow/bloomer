import * as React from 'react';
import { Bulma } from './../../bulma';
export interface Label<T> extends Bulma.Size, React.HTMLProps<T> {
}
export declare function Label(props: Label<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<Label<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
