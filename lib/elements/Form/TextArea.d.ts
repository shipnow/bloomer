import * as React from 'react';
import { Bulma } from './../../bulma';
export interface TextArea<T> extends Bulma.Size, Bulma.State, React.HTMLProps<T> {
}
export declare function TextArea(props: TextArea<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<TextArea<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
