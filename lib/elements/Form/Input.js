"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Input(props) {
    var className = classNames('input', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers, bulma_1.getStateModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps, bulma_1.removeStateProps);
    return React.createElement("input", tslib_1.__assign({}, HTMLProps, { className: className, type: props.type || 'text' }));
}
exports.Input = Input;
var HOC = bulma_1.withHelpersModifiers(Input);
exports.default = HOC;
//# sourceMappingURL=Input.js.map