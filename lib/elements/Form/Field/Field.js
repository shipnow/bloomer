"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
var helpers_1 = require("./../../../helpers");
var getModifier = function (modifier, helper, isDirection) {
    var _a, _b;
    if (modifier === true) {
        return _a = {}, _a["" + helper] = true, _a;
    }
    else if (typeof modifier === 'string') {
        return isDirection(modifier) ? (_b = {}, _b[helper + " " + helper + "-" + modifier] = true, _b) : {};
    }
    return {};
};
function Field(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field', tslib_1.__assign({}, getModifier(props.isGrouped, 'is-grouped', helpers_1.isOption(bulma_1.isRight, bulma_1.isCentered)), getModifier(props.hasAddons, 'has-addons', helpers_1.isOption(bulma_1.isRight, bulma_1.isCentered, bulma_1.isFullWidth)), { 'is-horizontal': props.isHorizontal }), props.className);
    var isGrouped = props.isGrouped, hasAddons = props.hasAddons, isHorizontal = props.isHorizontal, HTMLProps = tslib_1.__rest(props, ["isGrouped", "hasAddons", "isHorizontal"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Field = Field;
var HOC = bulma_1.withHelpersModifiers(Field);
exports.default = HOC;
//# sourceMappingURL=Field.js.map