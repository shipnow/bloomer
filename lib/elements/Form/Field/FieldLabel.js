"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
var helpers_1 = require("./../../../helpers");
function FieldLabel(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field-label', tslib_1.__assign({ 'is-normal': props.isNormal }, bulma_1.getSizeModifiers(props)), props.className);
    var isNormal = props.isNormal, rest = tslib_1.__rest(props, ["isNormal"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.FieldLabel = FieldLabel;
var HOC = bulma_1.withHelpersModifiers(FieldLabel);
exports.default = HOC;
//# sourceMappingURL=FieldLabel.js.map