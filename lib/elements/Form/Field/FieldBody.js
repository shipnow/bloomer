"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../../bulma");
function FieldBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.FieldBody = FieldBody;
var HOC = bulma_1.withHelpersModifiers(FieldBody);
exports.default = HOC;
//# sourceMappingURL=FieldBody.js.map