import * as React from 'react';
import { Bulma } from './../../bulma';
export declare function Radio(props: React.HTMLProps<HTMLElement>): JSX.Element;
declare const HOC: React.StatelessComponent<React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
