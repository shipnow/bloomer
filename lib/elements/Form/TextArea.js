"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function TextArea(props) {
    var className = classNames('textarea', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getSizeModifiers, bulma_1.getStateModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps, bulma_1.removeStateProps);
    return (React.createElement("textarea", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.TextArea = TextArea;
var HOC = bulma_1.withHelpersModifiers(TextArea);
exports.default = HOC;
//# sourceMappingURL=TextArea.js.map