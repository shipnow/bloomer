"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Label(props) {
    var className = classNames('label', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    return (React.createElement("label", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.Label = Label;
var HOC = bulma_1.withHelpersModifiers(Label);
exports.default = HOC;
//# sourceMappingURL=Label.js.map