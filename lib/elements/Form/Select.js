"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
function Select(props) {
    var wrapperClassName = classNames('select', tslib_1.__assign({ 'is-disabled': props.disabled }, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers, bulma_1.getLoadingModifiers)), props.className);
    var _a = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps, bulma_1.removeLoadingProps), children = _a.children, className = _a.className, HTMLProps = tslib_1.__rest(_a, ["children", "className"]);
    return (React.createElement("div", { className: wrapperClassName },
        React.createElement("select", tslib_1.__assign({}, HTMLProps), children)));
}
exports.Select = Select;
var HOC = bulma_1.withHelpersModifiers(Select);
exports.default = HOC;
//# sourceMappingURL=Select.js.map