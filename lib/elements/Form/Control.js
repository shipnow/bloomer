"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
var helpers_1 = require("./../../helpers");
var isDirection = helpers_1.isOption(bulma_1.isLeft, bulma_1.isRight);
var getModifier = function (modifier) {
    var _a;
    if (modifier === true) {
        return { 'has-icons-left has-icons-right': true };
    }
    else if (typeof modifier === 'string') {
        return isDirection(modifier) ? (_a = {}, _a["has-icons-" + modifier] = true, _a) : {};
    }
    else if (Array.isArray(modifier)) {
        return modifier.map(function (str) { return str.toLowerCase().trim(); })
            .reduce(function (init, option) {
            var _a;
            return isDirection(option) ? tslib_1.__assign({}, init, (_a = {}, _a["has-icons-" + option] = true, _a)) : init;
        }, {});
    }
    return {};
};
function Control(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('control', tslib_1.__assign({}, getModifier(props.hasIcons), { 'is-expanded': props.isExpanded }, bulma_1.getLoadingModifiers(props)), props.className);
    var hasIcons = props.hasIcons, isExpanded = props.isExpanded, rest = tslib_1.__rest(props, ["hasIcons", "isExpanded"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeLoadingProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Control = Control;
var HOC = bulma_1.withHelpersModifiers(Control);
exports.default = HOC;
//# sourceMappingURL=Control.js.map