"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../../bulma");
function Checkbox(props) {
    var wrapperClassName = classNames('checkbox', props.className);
    var children = props.children, className = props.className, HTMLProps = tslib_1.__rest(props, ["children", "className"]);
    return (React.createElement("label", { className: wrapperClassName },
        React.createElement("input", tslib_1.__assign({}, HTMLProps, { type: "checkbox" })),
        children));
}
exports.Checkbox = Checkbox;
var HOC = bulma_1.withHelpersModifiers(Checkbox);
exports.default = HOC;
//# sourceMappingURL=Checkbox.js.map