"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
var isRatio = helpers_1.is({
    '16:9': '16by9',
    '1:1': '1by1',
    '2:1': '2by1',
    '3:2': '3by2',
    '4:3': '4by3',
    'square': 'square',
});
var isSize = helpers_1.is({
    '128x128': true,
    '16x16': true,
    '24x24': true,
    '32x32': true,
    '48x48': true,
    '64x64': true,
    '96x96': true,
});
function getSizeModifiers(_a) {
    var size = _a.isSize;
    var _b;
    return tslib_1.__assign({}, (isSize(size) ? (_b = {}, _b["is-" + size] = true, _b) : {}));
}
function getRatioModifiers(_a) {
    var ratio = _a.isRatio;
    var _b;
    return tslib_1.__assign({}, (isRatio(ratio) ? (_b = {}, _b["is-" + isRatio(ratio)] = true, _b) : {}));
}
function removeImageProps(props) {
    var isSize = props.isSize, isRatio = props.isRatio, rest = tslib_1.__rest(props, ["isSize", "isRatio"]);
    return rest;
}
function Image(props) {
    var className = classNames('image', tslib_1.__assign({}, helpers_1.combineModifiers(props, getSizeModifiers, getRatioModifiers)), props.className);
    var _a = helpers_1.getHTMLProps(props, removeImageProps), children = _a.children, src = _a.src, HTMLProps = tslib_1.__rest(_a, ["children", "src"]);
    return (React.createElement("figure", tslib_1.__assign({}, HTMLProps, { className: className }),
        React.createElement("img", { src: src })));
}
exports.Image = Image;
var HOC = bulma_1.withHelpersModifiers(Image);
exports.default = HOC;
//# sourceMappingURL=Image.js.map