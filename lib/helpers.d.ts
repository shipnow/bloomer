/// <reference types="react" />
export declare function getHTMLProps(props: object, ...args: Function[]): React.HTMLProps<HTMLElement>;
export declare function combineModifiers(props: object, ...args: Function[]): object;
export declare const isBetween: (min: number, max: number) => (value: number) => boolean;
export declare const is: (options: object) => (str: string) => boolean;
export declare const isOption: (...fn: Function[]) => (str: string | boolean) => boolean;
