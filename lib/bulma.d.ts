import * as React from 'react';
import { Grid } from './grid/grid';
export declare namespace Bulma {
    interface Alignment {
        isAlign?: string;
    }
    type Sizes = 'large' | 'medium' | 'small';
    interface Size {
        isSize?: Sizes;
    }
    interface Tag {
        tag?: keyof React.ReactHTML;
    }
    interface FullWidth {
        isFullWidth?: boolean;
    }
    interface Active {
        isActive?: boolean;
    }
    interface Hovered {
        isHovered?: boolean;
    }
    interface Focused {
        isFocused?: boolean;
    }
    interface State extends Active, Hovered, Focused {
    }
    interface Loading {
        isLoading?: boolean;
    }
    interface Color {
        isColor?: string;
    }
    type HeadingSizes = 1 | 2 | 3 | 4 | 5 | 6;
    interface Heading {
        isSize?: HeadingSizes;
        isSpaced?: boolean;
    }
    interface Grid extends Grid.HorizontalSize, Grid.Offset {
    }
    type Platform = 'mobile' | 'tablet' | 'touch' | 'desktop' | 'widescreen';
    type PlatformOnly = 'tablet-only' | 'desktop-only';
    type AllPlatforms = Platform | PlatformOnly;
    type AllPlatformsWithDefault = AllPlatforms | 'default';
    type FlexDisplay = 'flex' | 'flex-mobile' | 'flex-tablet' | 'flex-touch' | 'flex-desktop' | 'flex-widescreen' | 'flex-tablet-only' | 'flex-desktop-only';
    type BlockDisplay = 'block' | 'block-mobile' | 'block-tablet' | 'block-touch' | 'block-desktop' | 'block-widescreen' | 'block-tablet-only' | 'block-desktop-only';
    type InlineDisplay = 'inline' | 'inline-mobile' | 'inline-tablet' | 'inline-touch' | 'inline-desktop' | 'inline-widescreen' | 'inline-tablet-only' | 'inline-desktop-only';
    type InlineBlockDisplay = 'inline-block' | 'inline-block-mobile' | 'inline-block-tablet' | 'inline-block-touch' | 'inline-block-desktop' | 'inline-block-widescreen' | 'inline-block-tablet-only' | 'inline-block-desktop-only';
    type InlineFlexDisplay = 'inline-flex' | 'inline-flex-mobile' | 'inline-flex-tablet' | 'inline-flex-touch' | 'inline-flex-desktop' | 'inline-flex-widescreen' | 'inline-flex-tablet-only' | 'inline-flex-desktop-only';
    type Displays = FlexDisplay | BlockDisplay | InlineDisplay | InlineBlockDisplay | InlineFlexDisplay;
    interface DisplayObject {
        flex?: AllPlatformsWithDefault | AllPlatformsWithDefault[];
        block?: AllPlatformsWithDefault | AllPlatformsWithDefault[];
        inline?: AllPlatformsWithDefault | AllPlatformsWithDefault[];
        'inline-block'?: AllPlatformsWithDefault | AllPlatformsWithDefault[];
        'inline-flex'?: AllPlatformsWithDefault | AllPlatformsWithDefault[];
    }
    interface Show {
        isDisplay?: Displays | Displays[] | DisplayObject;
    }
    interface Hide {
        isHidden?: boolean | AllPlatforms | AllPlatforms[];
    }
    interface Responsive extends Show, Hide {
    }
    type Alignments = 'left' | 'right';
    interface Helpers extends FullWidth, Responsive {
        isClearfix?: boolean;
        isPulled?: Alignments;
        isOverlay?: boolean;
        isMarginless?: boolean;
        isPaddingless?: boolean;
        isUnselectable?: boolean;
        hasTextAlign?: Alignments | 'centered';
        hasTextColor?: string;
    }
    interface Modifiers extends Alignment, Size, FullWidth, State, Color, Helpers {
    }
    interface Render {
        render?: Function;
    }
    interface NonHTMLProps extends Modifiers, Render {
    }
    type Component<T> = React.ComponentClass<T & React.HTMLProps<HTMLElement>> | React.SFC<T & React.HTMLProps<HTMLElement>>;
}
export declare const isMobile: (str: string) => boolean;
export declare const isTablet: (str: string) => boolean;
export declare const isTouch: (str: string) => boolean;
export declare const isDesktop: (str: string) => boolean;
export declare const isWidescreen: (str: string) => boolean;
export declare const isFullHD: (str: string) => boolean;
export declare const isLeft: (str: string) => boolean;
export declare const isRight: (str: string) => boolean;
export declare const isCentered: (str: string) => boolean;
export declare const isCenter: (str: string) => boolean;
export declare const isFullWidth: (str: string) => boolean;
export declare function getAlignmentModifiers({ isAlign: align }: Bulma.Alignment): {
    [x: string]: boolean;
};
export declare function removeAlignmentProps(props: Bulma.Alignment): {};
export declare function getSizeModifiers({ isSize: size }: Bulma.Size): {
    [x: string]: boolean;
};
export declare function removeSizeProps(props: Bulma.Size): {};
export declare function getActiveModifiers(props: Bulma.Active): {
    'is-active': boolean;
};
export declare function removeActiveModifiers(props: Bulma.Active): {};
export declare function getFocusedModifiers(props: Bulma.Focused): {
    'is-focused': boolean;
};
export declare function removeFocusedModifiers(props: Bulma.Focused): {};
export declare function getHoveredModifiers(props: Bulma.Hovered): {
    'is-hovered': boolean;
};
export declare function removeHoveredModifiers(props: Bulma.Hovered): {};
export declare function getStateModifiers(props: Bulma.State): {
    'is-hovered': boolean;
    'is-focused': boolean;
    'is-active': boolean;
};
export declare function removeStateProps(props: Bulma.State): {};
export declare function getLoadingModifiers(props: Bulma.Loading): {
    'is-loading': boolean;
};
export declare function removeLoadingProps(props: Bulma.Loading): {};
export declare function getColorModifiers({ isColor: color }: Bulma.Color): {
    [x: string]: boolean;
};
export declare function removeColorProps(props: Bulma.Color): {};
export declare function getHeadingModifiers({ isSpaced, isSize: size }: Bulma.Heading): {
    'is-spaced': boolean;
};
export declare function removeHeadingProps(props: Bulma.Heading): {};
export declare function withHelpersModifiers<T>(Component: Bulma.Component<T>): React.StatelessComponent<T & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
