"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
function getHTMLProps(props) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return args.length > 0 ? args.reduce(function (rest, fn) { return (tslib_1.__assign({}, fn(rest))); }, props) : props;
}
exports.getHTMLProps = getHTMLProps;
function combineModifiers(props) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return args.length > 0 ? args.reduce(function (rest, fn) { return (tslib_1.__assign({}, rest, fn(props))); }, {}) : {};
}
exports.combineModifiers = combineModifiers;
exports.isBetween = function (min, max) { return function (value) { return (value >= min && value <= max); }; };
exports.is = function (options) { return function (str) { return options[str] || false; }; };
exports.isOption = function () {
    var fn = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        fn[_i] = arguments[_i];
    }
    return function (str) { return fn.some(function (option) { return option(str); }); };
};
//# sourceMappingURL=helpers.js.map