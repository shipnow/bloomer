"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var helpers_1 = require("./helpers");
exports.isMobile = helpers_1.is({ mobile: true });
exports.isTablet = helpers_1.is({ tablet: true });
exports.isTouch = helpers_1.is({ touch: true });
exports.isDesktop = helpers_1.is({ desktop: true });
exports.isWidescreen = helpers_1.is({ widescreen: true });
exports.isFullHD = helpers_1.is({ fullhd: true });
var isTabletOnly = helpers_1.is({ 'tablet-only': true });
var isDesktopOnly = helpers_1.is({ 'desktop-only': true });
exports.isLeft = helpers_1.is({ left: true });
exports.isRight = helpers_1.is({ right: true });
exports.isCentered = helpers_1.is({ centered: true });
exports.isCenter = helpers_1.is({ center: true });
exports.isFullWidth = helpers_1.is({ fullwidth: true });
var isSmall = helpers_1.is({ small: true });
var isMedium = helpers_1.is({ medium: true });
var isLarge = helpers_1.is({ large: true });
var isAllPlatforms = helpers_1.isOption(exports.isMobile, exports.isTablet, exports.isDesktop, exports.isTouch, exports.isWidescreen, isTabletOnly, isDesktopOnly);
var isAlign = helpers_1.isOption(exports.isLeft, exports.isCentered, exports.isRight);
var isSize = helpers_1.isOption(isSmall, isMedium, isLarge);
function getAlignmentModifiers(_a) {
    var align = _a.isAlign;
    var _b;
    return isAlign(align) ? (_b = {}, _b["is-" + align] = true, _b) : {};
}
exports.getAlignmentModifiers = getAlignmentModifiers;
function removeAlignmentProps(props) {
    var isAlign = props.isAlign, rest = tslib_1.__rest(props, ["isAlign"]);
    return rest;
}
exports.removeAlignmentProps = removeAlignmentProps;
function getSizeModifiers(_a) {
    var size = _a.isSize;
    var _b;
    return isSize(size) ? (_b = {}, _b["is-" + size] = true, _b) : {};
}
exports.getSizeModifiers = getSizeModifiers;
function removeSizeProps(props) {
    var isSize = props.isSize, rest = tslib_1.__rest(props, ["isSize"]);
    return rest;
}
exports.removeSizeProps = removeSizeProps;
function getFullWidthModifiers(props) {
    return {
        'is-fullwidth': props.isFullWidth,
    };
}
function removeFullWidthProps(props) {
    var isFullWidth = props.isFullWidth, rest = tslib_1.__rest(props, ["isFullWidth"]);
    return rest;
}
function getActiveModifiers(props) {
    return { 'is-active': props.isActive };
}
exports.getActiveModifiers = getActiveModifiers;
function removeActiveModifiers(props) {
    var isActive = props.isActive, rest = tslib_1.__rest(props, ["isActive"]);
    return rest;
}
exports.removeActiveModifiers = removeActiveModifiers;
function getFocusedModifiers(props) {
    return { 'is-focused': props.isFocused };
}
exports.getFocusedModifiers = getFocusedModifiers;
function removeFocusedModifiers(props) {
    var isFocused = props.isFocused, rest = tslib_1.__rest(props, ["isFocused"]);
    return rest;
}
exports.removeFocusedModifiers = removeFocusedModifiers;
function getHoveredModifiers(props) {
    return { 'is-hovered': props.isHovered };
}
exports.getHoveredModifiers = getHoveredModifiers;
function removeHoveredModifiers(props) {
    var isHovered = props.isHovered, rest = tslib_1.__rest(props, ["isHovered"]);
    return rest;
}
exports.removeHoveredModifiers = removeHoveredModifiers;
function getStateModifiers(props) {
    return tslib_1.__assign({}, getActiveModifiers(props), getFocusedModifiers(props), getHoveredModifiers(props));
}
exports.getStateModifiers = getStateModifiers;
function removeStateProps(props) {
    var isActive = props.isActive, isFocused = props.isFocused, isHovered = props.isHovered, rest = tslib_1.__rest(props, ["isActive", "isFocused", "isHovered"]);
    return rest;
}
exports.removeStateProps = removeStateProps;
function getLoadingModifiers(props) {
    return {
        'is-loading': props.isLoading,
    };
}
exports.getLoadingModifiers = getLoadingModifiers;
function removeLoadingProps(props) {
    var isLoading = props.isLoading, rest = tslib_1.__rest(props, ["isLoading"]);
    return rest;
}
exports.removeLoadingProps = removeLoadingProps;
function getColorModifiers(_a) {
    var color = _a.isColor;
    var _b;
    return color ? (_b = {}, _b["is-" + color] = true, _b) : {};
}
exports.getColorModifiers = getColorModifiers;
function removeColorProps(props) {
    var isColor = props.isColor, rest = tslib_1.__rest(props, ["isColor"]);
    return rest;
}
exports.removeColorProps = removeColorProps;
var isValidHeading = helpers_1.isBetween(1, 6);
function getHeadingModifiers(_a) {
    var isSpaced = _a.isSpaced, size = _a.isSize;
    var _b;
    var isSize = isValidHeading(size) ? (_b = {}, _b["is-" + size] = true, _b) : {};
    return tslib_1.__assign({}, isSize, { 'is-spaced': isSpaced });
}
exports.getHeadingModifiers = getHeadingModifiers;
function removeHeadingProps(props) {
    var isSize = props.isSize, isSpaced = props.isSpaced, rest = tslib_1.__rest(props, ["isSize", "isSpaced"]);
    return rest;
}
exports.removeHeadingProps = removeHeadingProps;
var isFlex = helpers_1.is({ flex: true });
var isBlock = helpers_1.is({ block: true });
var isInline = helpers_1.is({ inline: true });
var isInlineBlock = helpers_1.is({ 'inline-block': true });
var isInlineFlex = helpers_1.is({ 'inline-flex': true });
var isDisplay = helpers_1.isOption(isFlex, isBlock, isInline, isInlineBlock, isInlineFlex);
var getShowModifiers = function (display) {
    var _a;
    var isDefault = function (str) { return str === 'default' ? true : false; };
    if (typeof display === 'string')
        return _a = {}, _a["is-" + display] = true, _a;
    if (Array.isArray(display))
        return display.reduce(function (acc, display) {
            var _a;
            return (tslib_1.__assign({}, acc, (_a = {}, _a["is-" + display] = true, _a)));
        }, {});
    if (typeof display === 'object') {
        return Object.keys(display).reduce(function (acc, key) {
            var _a, _b;
            if (Array.isArray(display[key])) {
                return display[key].reduce(function (acc, display) {
                    var _a, _b;
                    if (isDefault(display))
                        return tslib_1.__assign({}, acc, (_a = {}, _a["is-" + key] = true, _a));
                    return tslib_1.__assign({}, acc, (_b = {}, _b["is-" + key + "-" + display] = true, _b));
                }, acc);
            }
            if (isDefault(display[key]))
                return tslib_1.__assign({}, acc, (_a = {}, _a["is-" + key] = true, _a));
            return (isDisplay(key) && isAllPlatforms(display[key])) ? tslib_1.__assign({}, acc, (_b = {}, _b["is-" + key + "-" + display[key]] = true, _b)) : acc;
        }, {});
    }
    return {};
};
var getHideModifiers = function (platform) {
    var _a;
    if (typeof platform === 'boolean')
        return platform ? { 'is-hidden': true } : {};
    if (typeof platform === 'string')
        return isAllPlatforms(platform) ? (_a = {}, _a["is-hidden-" + platform] = true, _a) : {};
    if (Array.isArray(platform)) {
        return platform.reduce(function (acc, platform) {
            var _a;
            return isAllPlatforms(platform) ? tslib_1.__assign({}, acc, (_a = {}, _a["is-hidden-" + platform] = true, _a)) : acc;
        }, {});
    }
    return {};
};
var getAlignModifier = function (modifier, helper) {
    var _a;
    return isAlign(modifier) ? (_a = {}, _a[helper + "-" + modifier] = true, _a) : {};
};
var getColorModifier = function (modifier) {
    var _a;
    return modifier ? (_a = {}, _a["has-text-" + modifier] = true, _a) : {};
};
function getHelpersModifiers(_a) {
    var isDisplay = _a.isDisplay, isHidden = _a.isHidden, isPulled = _a.isPulled, isClearfix = _a.isClearfix, isOverlay = _a.isOverlay, isMarginless = _a.isMarginless, isPaddingless = _a.isPaddingless, isUnselectable = _a.isUnselectable, hasTextAlign = _a.hasTextAlign, hasTextColor = _a.hasTextColor;
    return tslib_1.__assign({}, getShowModifiers(isDisplay), getHideModifiers(isHidden), getAlignModifier(isPulled, 'is-pulled'), getAlignModifier(hasTextAlign, 'has-text'), getColorModifier(hasTextColor), { 'is-clearfix': isClearfix, 'is-marginless': isMarginless, 'is-overlay': isOverlay, 'is-paddingless': isPaddingless, 'is-unselectable': isUnselectable });
}
function removeHelpersProps(props) {
    var isDisplay = props.isDisplay, isHidden = props.isHidden, isClearfix = props.isClearfix, isPulled = props.isPulled, isOverlay = props.isOverlay, isMarginless = props.isMarginless, isPaddingless = props.isPaddingless, isUnselectable = props.isUnselectable, hasTextAlign = props.hasTextAlign, hasTextColor = props.hasTextColor, rest = tslib_1.__rest(props, ["isDisplay", "isHidden", "isClearfix", "isPulled", "isOverlay", "isMarginless", "isPaddingless", "isUnselectable", "hasTextAlign", "hasTextColor"]);
    return rest;
}
function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name;
}
function withHelpersModifiers(Component) {
    var SFC = function (props) {
        var className = classNames(tslib_1.__assign({}, helpers_1.combineModifiers(props, getHelpersModifiers, getFullWidthModifiers)), props.className);
        var rest = helpers_1.getHTMLProps(props, removeHelpersProps, removeFullWidthProps);
        return className ? React.createElement(Component, tslib_1.__assign({}, rest, { className: className })) : React.createElement(Component, tslib_1.__assign({}, rest));
    };
    SFC.displayName = "withHelpersModifiers(" + getDisplayName(Component) + ")";
    return SFC;
}
exports.withHelpersModifiers = withHelpersModifiers;
//# sourceMappingURL=bulma.js.map