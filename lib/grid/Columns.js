"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
function Columns(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('columns', {
        'is-centered': props.isCentered,
        'is-desktop': props.isDesktop,
        'is-gapless': props.isGapless,
        'is-grid': props.isGrid,
        'is-mobile': props.isMobile,
        'is-multiline': props.isMultiline,
        'is-vcentered': props.isVCentered,
    }, props.className);
    var isMobile = props.isMobile, isDesktop = props.isDesktop, isGapless = props.isGapless, isGrid = props.isGrid, isMultiline = props.isMultiline, isVCentered = props.isVCentered, isCentered = props.isCentered, HTMLProps = tslib_1.__rest(props, ["isMobile", "isDesktop", "isGapless", "isGrid", "isMultiline", "isVCentered", "isCentered"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Columns = Columns;
var HOC = bulma_1.withHelpersModifiers(Columns);
exports.default = HOC;
//# sourceMappingURL=Columns.js.map