"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
var grid_1 = require("./grid");
function Tile(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('tile', tslib_1.__assign({ 'is-ancestor': props.isAncestor, 'is-child': props.isChild, 'is-parent': props.isParent, 'is-vertical': props.isVertical }, grid_1.getGridSizesModifiers(props.isSize)), props.className);
    var isAncestor = props.isAncestor, isChild = props.isChild, isParent = props.isParent, isVertical = props.isVertical, rest = tslib_1.__rest(props, ["isAncestor", "isChild", "isParent", "isVertical"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, grid_1.removeGridSizesProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tile = Tile;
var HOC = bulma_1.withHelpersModifiers(Tile);
exports.default = HOC;
//# sourceMappingURL=Tile.js.map