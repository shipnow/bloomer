"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
var isValidSize = helpers_1.isBetween(1, 12);
var isPlatform = helpers_1.isOption(bulma_1.isMobile, bulma_1.isTablet, bulma_1.isTouch, bulma_1.isDesktop, bulma_1.isWidescreen, bulma_1.isFullHD);
function getGridSizesModifiers(isSize, isOffset, platform) {
    if (isOffset === void 0) { isOffset = false; }
    if (platform === void 0) { platform = false; }
    var _a;
    return (Number.isInteger(isSize) && isValidSize(isSize)) ? (_a = {},
        _a["is-" + (isOffset ? 'offset-' : '') + isSize + (isPlatform(platform) ? "-" + platform : '')] = true,
        _a) : {};
}
exports.getGridSizesModifiers = getGridSizesModifiers;
function removeGridSizesProps(props) {
    var isSize = props.isSize, rest = tslib_1.__rest(props, ["isSize"]);
    return rest;
}
exports.removeGridSizesProps = removeGridSizesProps;
var fractions = {
    '1/2': 'half',
    '1/3': 'one-third',
    '1/4': 'one-quarter',
    '2/3': 'two-thirds',
    '3/4': 'three-quarters',
};
var width = {
    full: 'full',
    narrow: 'narrow',
};
function getGridFractionsModifiers(size, isOffset, platform) {
    var _a;
    var sizes = !isOffset ? tslib_1.__assign({}, fractions, width) : fractions;
    return sizes[size] ? (_a = {}, _a["is-" + (isOffset ? 'offset-' : '') + sizes[size] + (isPlatform(platform) ? '-' + platform : '')] = true, _a) : {};
}
function getGridObjectSizeModifiers(size, isOffset) {
    return Object.keys(size).reduce(function (acc, key) {
        if (key === 'default')
            return tslib_1.__assign({}, acc, getHorizontalSizeModifiers(size[key], isOffset));
        return isPlatform(key) ? tslib_1.__assign({}, acc, getHorizontalSizeModifiers(size[key], isOffset, key)) : acc;
    }, {});
}
function getHorizontalSizeModifiers(size, isOffset, platform) {
    if (isOffset === void 0) { isOffset = false; }
    if (platform === void 0) { platform = false; }
    if (typeof size === 'number')
        return getGridSizesModifiers(size, isOffset, platform);
    if (typeof size === 'string')
        return getGridFractionsModifiers(size, isOffset, platform);
    if (typeof size === 'object')
        return getGridObjectSizeModifiers(size, isOffset);
    return {};
}
function getSizeModifiers(props) {
    return getHorizontalSizeModifiers(props.isSize);
}
exports.getSizeModifiers = getSizeModifiers;
function removeSizeProps(props) {
    return removeGridSizesProps(props);
}
exports.removeSizeProps = removeSizeProps;
function getOffsetModifiers(props) {
    return getHorizontalSizeModifiers(props.isOffset, true);
}
exports.getOffsetModifiers = getOffsetModifiers;
function removeOffsetProps(props) {
    var isOffset = props.isOffset, rest = tslib_1.__rest(props, ["isOffset"]);
    return rest;
}
exports.removeOffsetProps = removeOffsetProps;
//# sourceMappingURL=grid.js.map