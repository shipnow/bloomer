import * as React from 'react';
import { Bulma } from './../bulma';
import { Grid } from './grid';
export interface Tile<T> extends Bulma.Render, Bulma.Tag, React.HTMLProps<T> {
    isSize?: Grid.Sizes;
    isAncestor?: boolean;
    isChild?: boolean;
    isParent?: boolean;
    isVertical?: boolean;
}
export declare function Tile({ tag, render, ...props }: Tile<HTMLElement>): any;
declare const HOC: React.StatelessComponent<Tile<HTMLElement> & React.HTMLProps<HTMLElement> & Bulma.Helpers>;
export default HOC;
