"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var classNames = require("classnames");
var React = require("react");
var bulma_1 = require("./../bulma");
var helpers_1 = require("./../helpers");
var grid_1 = require("./grid");
function Column(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('column', tslib_1.__assign({}, helpers_1.combineModifiers(props, grid_1.getSizeModifiers, grid_1.getOffsetModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, grid_1.removeSizeProps, grid_1.removeOffsetProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Column = Column;
var HOC = bulma_1.withHelpersModifiers(Column);
exports.default = HOC;
//# sourceMappingURL=Column.js.map