export declare namespace Grid {
    type Sizes = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
    type Fractions = '3/4' | '2/3' | '1/2' | '1/3' | '1/4';
    type Width = 'full' | 'narrow';
    type AllSizes = Sizes | Fractions | Width;
    interface SizeObject {
        mobile?: AllSizes;
        tablet?: AllSizes;
        touch?: AllSizes;
        desktop?: AllSizes;
        widescreen?: AllSizes;
        fullhd?: AllSizes;
        default?: AllSizes;
    }
    type Platforms = 'mobile' | 'tablet' | 'desktop';
    interface HorizontalSize {
        isSize?: AllSizes | SizeObject;
    }
    interface OffsetObject {
        mobile?: Sizes | Fractions;
        tablet?: Sizes | Fractions;
        touch?: Sizes | Fractions;
        desktop?: Sizes | Fractions;
        widescreen?: Sizes | Fractions;
        fullhd?: Sizes | Fractions;
        default?: Sizes | Fractions;
    }
    interface Offset {
        isOffset?: Sizes | Fractions | OffsetObject;
    }
    interface NonHTMLProps extends HorizontalSize, Offset {
    }
}
export declare function getGridSizesModifiers(isSize: any, isOffset?: boolean, platform?: boolean | string): {
    [x: string]: boolean;
};
export declare function removeGridSizesProps(props: any): any;
export declare function getSizeModifiers(props: any): any;
export declare function removeSizeProps(props: any): any;
export declare function getOffsetModifiers(props: any): any;
export declare function removeOffsetProps(props: any): any;
