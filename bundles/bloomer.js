(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("react"));
	else if(typeof define === 'function' && define.amd)
		define("Bloomer", ["react"], factory);
	else if(typeof exports === 'object')
		exports["Bloomer"] = factory(require("react"));
	else
		root["Bloomer"] = factory(root["React"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["__extends"] = __extends;
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (immutable) */ __webpack_exports__["__rest"] = __rest;
/* harmony export (immutable) */ __webpack_exports__["__decorate"] = __decorate;
/* harmony export (immutable) */ __webpack_exports__["__param"] = __param;
/* harmony export (immutable) */ __webpack_exports__["__metadata"] = __metadata;
/* harmony export (immutable) */ __webpack_exports__["__awaiter"] = __awaiter;
/* harmony export (immutable) */ __webpack_exports__["__generator"] = __generator;
/* harmony export (immutable) */ __webpack_exports__["__exportStar"] = __exportStar;
/* harmony export (immutable) */ __webpack_exports__["__values"] = __values;
/* harmony export (immutable) */ __webpack_exports__["__read"] = __read;
/* harmony export (immutable) */ __webpack_exports__["__spread"] = __spread;
/* harmony export (immutable) */ __webpack_exports__["__await"] = __await;
/* harmony export (immutable) */ __webpack_exports__["__asyncGenerator"] = __asyncGenerator;
/* harmony export (immutable) */ __webpack_exports__["__asyncDelegator"] = __asyncDelegator;
/* harmony export (immutable) */ __webpack_exports__["__asyncValues"] = __asyncValues;
/* harmony export (immutable) */ __webpack_exports__["__makeTemplateObject"] = __makeTemplateObject;
/* harmony export (immutable) */ __webpack_exports__["__importStar"] = __importStar;
/* harmony export (immutable) */ __webpack_exports__["__importDefault"] = __importDefault;
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var helpers_1 = __webpack_require__(4);
exports.isMobile = helpers_1.is({ mobile: true });
exports.isTablet = helpers_1.is({ tablet: true });
exports.isTouch = helpers_1.is({ touch: true });
exports.isDesktop = helpers_1.is({ desktop: true });
exports.isWidescreen = helpers_1.is({ widescreen: true });
exports.isFullHD = helpers_1.is({ fullhd: true });
var isTabletOnly = helpers_1.is({ 'tablet-only': true });
var isDesktopOnly = helpers_1.is({ 'desktop-only': true });
exports.isLeft = helpers_1.is({ left: true });
exports.isRight = helpers_1.is({ right: true });
exports.isCentered = helpers_1.is({ centered: true });
exports.isCenter = helpers_1.is({ center: true });
exports.isFullWidth = helpers_1.is({ fullwidth: true });
var isSmall = helpers_1.is({ small: true });
var isMedium = helpers_1.is({ medium: true });
var isLarge = helpers_1.is({ large: true });
var isAllPlatforms = helpers_1.isOption(exports.isMobile, exports.isTablet, exports.isDesktop, exports.isTouch, exports.isWidescreen, isTabletOnly, isDesktopOnly);
var isAlign = helpers_1.isOption(exports.isLeft, exports.isCentered, exports.isRight);
var isSize = helpers_1.isOption(isSmall, isMedium, isLarge);
function getAlignmentModifiers(_a) {
    var align = _a.isAlign;
    var _b;
    return isAlign(align) ? (_b = {}, _b["is-" + align] = true, _b) : {};
}
exports.getAlignmentModifiers = getAlignmentModifiers;
function removeAlignmentProps(props) {
    var isAlign = props.isAlign, rest = tslib_1.__rest(props, ["isAlign"]);
    return rest;
}
exports.removeAlignmentProps = removeAlignmentProps;
function getSizeModifiers(_a) {
    var size = _a.isSize;
    var _b;
    return isSize(size) ? (_b = {}, _b["is-" + size] = true, _b) : {};
}
exports.getSizeModifiers = getSizeModifiers;
function removeSizeProps(props) {
    var isSize = props.isSize, rest = tslib_1.__rest(props, ["isSize"]);
    return rest;
}
exports.removeSizeProps = removeSizeProps;
function getFullWidthModifiers(props) {
    return {
        'is-fullwidth': props.isFullWidth,
    };
}
function removeFullWidthProps(props) {
    var isFullWidth = props.isFullWidth, rest = tslib_1.__rest(props, ["isFullWidth"]);
    return rest;
}
function getActiveModifiers(props) {
    return { 'is-active': props.isActive };
}
exports.getActiveModifiers = getActiveModifiers;
function removeActiveModifiers(props) {
    var isActive = props.isActive, rest = tslib_1.__rest(props, ["isActive"]);
    return rest;
}
exports.removeActiveModifiers = removeActiveModifiers;
function getFocusedModifiers(props) {
    return { 'is-focused': props.isFocused };
}
exports.getFocusedModifiers = getFocusedModifiers;
function removeFocusedModifiers(props) {
    var isFocused = props.isFocused, rest = tslib_1.__rest(props, ["isFocused"]);
    return rest;
}
exports.removeFocusedModifiers = removeFocusedModifiers;
function getHoveredModifiers(props) {
    return { 'is-hovered': props.isHovered };
}
exports.getHoveredModifiers = getHoveredModifiers;
function removeHoveredModifiers(props) {
    var isHovered = props.isHovered, rest = tslib_1.__rest(props, ["isHovered"]);
    return rest;
}
exports.removeHoveredModifiers = removeHoveredModifiers;
function getStateModifiers(props) {
    return tslib_1.__assign({}, getActiveModifiers(props), getFocusedModifiers(props), getHoveredModifiers(props));
}
exports.getStateModifiers = getStateModifiers;
function removeStateProps(props) {
    var isActive = props.isActive, isFocused = props.isFocused, isHovered = props.isHovered, rest = tslib_1.__rest(props, ["isActive", "isFocused", "isHovered"]);
    return rest;
}
exports.removeStateProps = removeStateProps;
function getLoadingModifiers(props) {
    return {
        'is-loading': props.isLoading,
    };
}
exports.getLoadingModifiers = getLoadingModifiers;
function removeLoadingProps(props) {
    var isLoading = props.isLoading, rest = tslib_1.__rest(props, ["isLoading"]);
    return rest;
}
exports.removeLoadingProps = removeLoadingProps;
function getColorModifiers(_a) {
    var color = _a.isColor;
    var _b;
    return color ? (_b = {}, _b["is-" + color] = true, _b) : {};
}
exports.getColorModifiers = getColorModifiers;
function removeColorProps(props) {
    var isColor = props.isColor, rest = tslib_1.__rest(props, ["isColor"]);
    return rest;
}
exports.removeColorProps = removeColorProps;
var isValidHeading = helpers_1.isBetween(1, 6);
function getHeadingModifiers(_a) {
    var isSpaced = _a.isSpaced, size = _a.isSize;
    var _b;
    var isSize = isValidHeading(size) ? (_b = {}, _b["is-" + size] = true, _b) : {};
    return tslib_1.__assign({}, isSize, { 'is-spaced': isSpaced });
}
exports.getHeadingModifiers = getHeadingModifiers;
function removeHeadingProps(props) {
    var isSize = props.isSize, isSpaced = props.isSpaced, rest = tslib_1.__rest(props, ["isSize", "isSpaced"]);
    return rest;
}
exports.removeHeadingProps = removeHeadingProps;
var isFlex = helpers_1.is({ flex: true });
var isBlock = helpers_1.is({ block: true });
var isInline = helpers_1.is({ inline: true });
var isInlineBlock = helpers_1.is({ 'inline-block': true });
var isInlineFlex = helpers_1.is({ 'inline-flex': true });
var isDisplay = helpers_1.isOption(isFlex, isBlock, isInline, isInlineBlock, isInlineFlex);
var getShowModifiers = function (display) {
    var _a;
    var isDefault = function (str) { return str === 'default' ? true : false; };
    if (typeof display === 'string')
        return _a = {}, _a["is-" + display] = true, _a;
    if (Array.isArray(display))
        return display.reduce(function (acc, display) {
            var _a;
            return (tslib_1.__assign({}, acc, (_a = {}, _a["is-" + display] = true, _a)));
        }, {});
    if (typeof display === 'object') {
        return Object.keys(display).reduce(function (acc, key) {
            var _a, _b;
            if (Array.isArray(display[key])) {
                return display[key].reduce(function (acc, display) {
                    var _a, _b;
                    if (isDefault(display))
                        return tslib_1.__assign({}, acc, (_a = {}, _a["is-" + key] = true, _a));
                    return tslib_1.__assign({}, acc, (_b = {}, _b["is-" + key + "-" + display] = true, _b));
                }, acc);
            }
            if (isDefault(display[key]))
                return tslib_1.__assign({}, acc, (_a = {}, _a["is-" + key] = true, _a));
            return (isDisplay(key) && isAllPlatforms(display[key])) ? tslib_1.__assign({}, acc, (_b = {}, _b["is-" + key + "-" + display[key]] = true, _b)) : acc;
        }, {});
    }
    return {};
};
var getHideModifiers = function (platform) {
    var _a;
    if (typeof platform === 'boolean')
        return platform ? { 'is-hidden': true } : {};
    if (typeof platform === 'string')
        return isAllPlatforms(platform) ? (_a = {}, _a["is-hidden-" + platform] = true, _a) : {};
    if (Array.isArray(platform)) {
        return platform.reduce(function (acc, platform) {
            var _a;
            return isAllPlatforms(platform) ? tslib_1.__assign({}, acc, (_a = {}, _a["is-hidden-" + platform] = true, _a)) : acc;
        }, {});
    }
    return {};
};
var getAlignModifier = function (modifier, helper) {
    var _a;
    return isAlign(modifier) ? (_a = {}, _a[helper + "-" + modifier] = true, _a) : {};
};
var getColorModifier = function (modifier) {
    var _a;
    return modifier ? (_a = {}, _a["has-text-" + modifier] = true, _a) : {};
};
function getHelpersModifiers(_a) {
    var isDisplay = _a.isDisplay, isHidden = _a.isHidden, isPulled = _a.isPulled, isClearfix = _a.isClearfix, isOverlay = _a.isOverlay, isMarginless = _a.isMarginless, isPaddingless = _a.isPaddingless, isUnselectable = _a.isUnselectable, hasTextAlign = _a.hasTextAlign, hasTextColor = _a.hasTextColor;
    return tslib_1.__assign({}, getShowModifiers(isDisplay), getHideModifiers(isHidden), getAlignModifier(isPulled, 'is-pulled'), getAlignModifier(hasTextAlign, 'has-text'), getColorModifier(hasTextColor), { 'is-clearfix': isClearfix, 'is-marginless': isMarginless, 'is-overlay': isOverlay, 'is-paddingless': isPaddingless, 'is-unselectable': isUnselectable });
}
function removeHelpersProps(props) {
    var isDisplay = props.isDisplay, isHidden = props.isHidden, isClearfix = props.isClearfix, isPulled = props.isPulled, isOverlay = props.isOverlay, isMarginless = props.isMarginless, isPaddingless = props.isPaddingless, isUnselectable = props.isUnselectable, hasTextAlign = props.hasTextAlign, hasTextColor = props.hasTextColor, rest = tslib_1.__rest(props, ["isDisplay", "isHidden", "isClearfix", "isPulled", "isOverlay", "isMarginless", "isPaddingless", "isUnselectable", "hasTextAlign", "hasTextColor"]);
    return rest;
}
function getDisplayName(WrappedComponent) {
    return WrappedComponent.displayName || WrappedComponent.name;
}
function withHelpersModifiers(Component) {
    var SFC = function (props) {
        var className = classNames(tslib_1.__assign({}, helpers_1.combineModifiers(props, getHelpersModifiers, getFullWidthModifiers)), props.className);
        var rest = helpers_1.getHTMLProps(props, removeHelpersProps, removeFullWidthProps);
        return className ? React.createElement(Component, tslib_1.__assign({}, rest, { className: className })) : React.createElement(Component, tslib_1.__assign({}, rest));
    };
    SFC.displayName = "withHelpersModifiers(" + getDisplayName(Component) + ")";
    return SFC;
}
exports.withHelpersModifiers = withHelpersModifiers;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2017 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg) && arg.length) {
				var inner = classNames.apply(null, arg);
				if (inner) {
					classes.push(inner);
				}
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
function getHTMLProps(props) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return args.length > 0 ? args.reduce(function (rest, fn) { return (tslib_1.__assign({}, fn(rest))); }, props) : props;
}
exports.getHTMLProps = getHTMLProps;
function combineModifiers(props) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    return args.length > 0 ? args.reduce(function (rest, fn) { return (tslib_1.__assign({}, rest, fn(props))); }, {}) : {};
}
exports.combineModifiers = combineModifiers;
exports.isBetween = function (min, max) { return function (value) { return (value >= min && value <= max); }; };
exports.is = function (options) { return function (str) { return options[str] || false; }; };
exports.isOption = function () {
    var fn = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        fn[_i] = arguments[_i];
    }
    return function (str) { return fn.some(function (option) { return option(str); }); };
};


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var isValidSize = helpers_1.isBetween(1, 12);
var isPlatform = helpers_1.isOption(bulma_1.isMobile, bulma_1.isTablet, bulma_1.isTouch, bulma_1.isDesktop, bulma_1.isWidescreen, bulma_1.isFullHD);
function getGridSizesModifiers(isSize, isOffset, platform) {
    if (isOffset === void 0) { isOffset = false; }
    if (platform === void 0) { platform = false; }
    var _a;
    return (Number.isInteger(isSize) && isValidSize(isSize)) ? (_a = {},
        _a["is-" + (isOffset ? 'offset-' : '') + isSize + (isPlatform(platform) ? "-" + platform : '')] = true,
        _a) : {};
}
exports.getGridSizesModifiers = getGridSizesModifiers;
function removeGridSizesProps(props) {
    var isSize = props.isSize, rest = tslib_1.__rest(props, ["isSize"]);
    return rest;
}
exports.removeGridSizesProps = removeGridSizesProps;
var fractions = {
    '1/2': 'half',
    '1/3': 'one-third',
    '1/4': 'one-quarter',
    '2/3': 'two-thirds',
    '3/4': 'three-quarters',
};
var width = {
    full: 'full',
    narrow: 'narrow',
};
function getGridFractionsModifiers(size, isOffset, platform) {
    var _a;
    var sizes = !isOffset ? tslib_1.__assign({}, fractions, width) : fractions;
    return sizes[size] ? (_a = {}, _a["is-" + (isOffset ? 'offset-' : '') + sizes[size] + (isPlatform(platform) ? '-' + platform : '')] = true, _a) : {};
}
function getGridObjectSizeModifiers(size, isOffset) {
    return Object.keys(size).reduce(function (acc, key) {
        if (key === 'default')
            return tslib_1.__assign({}, acc, getHorizontalSizeModifiers(size[key], isOffset));
        return isPlatform(key) ? tslib_1.__assign({}, acc, getHorizontalSizeModifiers(size[key], isOffset, key)) : acc;
    }, {});
}
function getHorizontalSizeModifiers(size, isOffset, platform) {
    if (isOffset === void 0) { isOffset = false; }
    if (platform === void 0) { platform = false; }
    if (typeof size === 'number')
        return getGridSizesModifiers(size, isOffset, platform);
    if (typeof size === 'string')
        return getGridFractionsModifiers(size, isOffset, platform);
    if (typeof size === 'object')
        return getGridObjectSizeModifiers(size, isOffset);
    return {};
}
function getSizeModifiers(props) {
    return getHorizontalSizeModifiers(props.isSize);
}
exports.getSizeModifiers = getSizeModifiers;
function removeSizeProps(props) {
    return removeGridSizesProps(props);
}
exports.removeSizeProps = removeSizeProps;
function getOffsetModifiers(props) {
    return getHorizontalSizeModifiers(props.isOffset, true);
}
exports.getOffsetModifiers = getOffsetModifiers;
function removeOffsetProps(props) {
    var isOffset = props.isOffset, rest = tslib_1.__rest(props, ["isOffset"]);
    return rest;
}
exports.removeOffsetProps = removeOffsetProps;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(7);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Columns_1 = __webpack_require__(8);
exports.Columns = Columns_1.default;
var Column_1 = __webpack_require__(9);
exports.Column = Column_1.default;
var Tile_1 = __webpack_require__(10);
exports.Tile = Tile_1.default;
var Box_1 = __webpack_require__(11);
exports.Box = Box_1.default;
var Button_1 = __webpack_require__(12);
exports.Button = Button_1.default;
var Content_1 = __webpack_require__(13);
exports.Content = Content_1.default;
var Delete_1 = __webpack_require__(14);
exports.Delete = Delete_1.default;
var Icon_1 = __webpack_require__(15);
exports.Icon = Icon_1.default;
var Image_1 = __webpack_require__(16);
exports.Image = Image_1.default;
var Notification_1 = __webpack_require__(17);
exports.Notification = Notification_1.default;
var Progress_1 = __webpack_require__(18);
exports.Progress = Progress_1.default;
var Table_1 = __webpack_require__(19);
exports.Table = Table_1.default;
var Tag_1 = __webpack_require__(20);
exports.Tag = Tag_1.default;
var Title_1 = __webpack_require__(21);
exports.Title = Title_1.default;
var Subtitle_1 = __webpack_require__(22);
exports.Subtitle = Subtitle_1.default;
var Heading_1 = __webpack_require__(23);
exports.Heading = Heading_1.default;
var Checkbox_1 = __webpack_require__(24);
exports.Checkbox = Checkbox_1.default;
var Control_1 = __webpack_require__(25);
exports.Control = Control_1.default;
var Help_1 = __webpack_require__(26);
exports.Help = Help_1.default;
var Input_1 = __webpack_require__(27);
exports.Input = Input_1.default;
var Label_1 = __webpack_require__(28);
exports.Label = Label_1.default;
var Radio_1 = __webpack_require__(29);
exports.Radio = Radio_1.default;
var Select_1 = __webpack_require__(30);
exports.Select = Select_1.default;
var TextArea_1 = __webpack_require__(31);
exports.TextArea = TextArea_1.default;
var Field_1 = __webpack_require__(32);
exports.Field = Field_1.default;
var FieldBody_1 = __webpack_require__(33);
exports.FieldBody = FieldBody_1.default;
var FieldLabel_1 = __webpack_require__(34);
exports.FieldLabel = FieldLabel_1.default;
var Breadcrumb_1 = __webpack_require__(35);
exports.Breadcrumb = Breadcrumb_1.default;
var BreadcrumbItem_1 = __webpack_require__(36);
exports.BreadcrumbItem = BreadcrumbItem_1.default;
var Card_1 = __webpack_require__(37);
exports.Card = Card_1.default;
var CardImage_1 = __webpack_require__(38);
exports.CardImage = CardImage_1.default;
var CardContent_1 = __webpack_require__(39);
exports.CardContent = CardContent_1.default;
var CardHeader_1 = __webpack_require__(40);
exports.CardHeader = CardHeader_1.default;
var CardHeaderTitle_1 = __webpack_require__(41);
exports.CardHeaderTitle = CardHeaderTitle_1.default;
var CardHeaderIcon_1 = __webpack_require__(42);
exports.CardHeaderIcon = CardHeaderIcon_1.default;
var CardFooter_1 = __webpack_require__(43);
exports.CardFooter = CardFooter_1.default;
var CardFooterItem_1 = __webpack_require__(44);
exports.CardFooterItem = CardFooterItem_1.default;
var Dropdown_1 = __webpack_require__(45);
exports.Dropdown = Dropdown_1.default;
var DropdownContent_1 = __webpack_require__(46);
exports.DropdownContent = DropdownContent_1.default;
var DropdownDivider_1 = __webpack_require__(47);
exports.DropdownDivider = DropdownDivider_1.default;
var DropdownItem_1 = __webpack_require__(48);
exports.DropdownItem = DropdownItem_1.default;
var DropdownMenu_1 = __webpack_require__(49);
exports.DropdownMenu = DropdownMenu_1.default;
var DropdownTrigger_1 = __webpack_require__(50);
exports.DropdownTrigger = DropdownTrigger_1.default;
var Level_1 = __webpack_require__(51);
exports.Level = Level_1.default;
var LevelItem_1 = __webpack_require__(52);
exports.LevelItem = LevelItem_1.default;
var LevelLeft_1 = __webpack_require__(53);
exports.LevelLeft = LevelLeft_1.default;
var LevelRight_1 = __webpack_require__(54);
exports.LevelRight = LevelRight_1.default;
var Media_1 = __webpack_require__(55);
exports.Media = Media_1.default;
var MediaContent_1 = __webpack_require__(56);
exports.MediaContent = MediaContent_1.default;
var MediaLeft_1 = __webpack_require__(57);
exports.MediaLeft = MediaLeft_1.default;
var MediaRight_1 = __webpack_require__(58);
exports.MediaRight = MediaRight_1.default;
var Menu_1 = __webpack_require__(59);
exports.Menu = Menu_1.default;
var MenuLabel_1 = __webpack_require__(60);
exports.MenuLabel = MenuLabel_1.default;
var MenuList_1 = __webpack_require__(61);
exports.MenuList = MenuList_1.default;
var MenuLink_1 = __webpack_require__(62);
exports.MenuLink = MenuLink_1.default;
var Message_1 = __webpack_require__(63);
exports.Message = Message_1.default;
var MessageHeader_1 = __webpack_require__(64);
exports.MessageHeader = MessageHeader_1.default;
var MessageBody_1 = __webpack_require__(65);
exports.MessageBody = MessageBody_1.default;
var Modal_1 = __webpack_require__(66);
exports.Modal = Modal_1.default;
var ModalBackground_1 = __webpack_require__(67);
exports.ModalBackground = ModalBackground_1.default;
var ModalContent_1 = __webpack_require__(68);
exports.ModalContent = ModalContent_1.default;
var ModalClose_1 = __webpack_require__(69);
exports.ModalClose = ModalClose_1.default;
var ModalCard_1 = __webpack_require__(70);
exports.ModalCard = ModalCard_1.default;
var ModalCardHeader_1 = __webpack_require__(71);
exports.ModalCardHeader = ModalCardHeader_1.default;
var ModalCardTitle_1 = __webpack_require__(72);
exports.ModalCardTitle = ModalCardTitle_1.default;
var ModalCardBody_1 = __webpack_require__(73);
exports.ModalCardBody = ModalCardBody_1.default;
var ModalCardFooter_1 = __webpack_require__(74);
exports.ModalCardFooter = ModalCardFooter_1.default;
var Nav_1 = __webpack_require__(75);
exports.Nav = Nav_1.default;
var NavLeft_1 = __webpack_require__(76);
exports.NavLeft = NavLeft_1.default;
var NavCenter_1 = __webpack_require__(77);
exports.NavCenter = NavCenter_1.default;
var NavRight_1 = __webpack_require__(78);
exports.NavRight = NavRight_1.default;
var NavToggle_1 = __webpack_require__(79);
exports.NavToggle = NavToggle_1.default;
var NavItem_1 = __webpack_require__(80);
exports.NavItem = NavItem_1.default;
var Navbar_1 = __webpack_require__(81);
exports.Navbar = Navbar_1.default;
var NavbarBrand_1 = __webpack_require__(82);
exports.NavbarBrand = NavbarBrand_1.default;
var NavbarBurger_1 = __webpack_require__(83);
exports.NavbarBurger = NavbarBurger_1.default;
var NavbarMenu_1 = __webpack_require__(84);
exports.NavbarMenu = NavbarMenu_1.default;
var NavbarStart_1 = __webpack_require__(85);
exports.NavbarStart = NavbarStart_1.default;
var NavbarEnd_1 = __webpack_require__(86);
exports.NavbarEnd = NavbarEnd_1.default;
var NavbarItem_1 = __webpack_require__(87);
exports.NavbarItem = NavbarItem_1.default;
var NavbarLink_1 = __webpack_require__(88);
exports.NavbarLink = NavbarLink_1.default;
var NavbarDropdown_1 = __webpack_require__(89);
exports.NavbarDropdown = NavbarDropdown_1.default;
var NavbarDivider_1 = __webpack_require__(90);
exports.NavbarDivider = NavbarDivider_1.default;
var Pagination_1 = __webpack_require__(91);
exports.Pagination = Pagination_1.default;
var PageControl_1 = __webpack_require__(92);
exports.PageControl = PageControl_1.default;
var Ellipsis_1 = __webpack_require__(93);
exports.PageEllipsis = Ellipsis_1.default;
var Page_1 = __webpack_require__(94);
exports.Page = Page_1.default;
var PageList_1 = __webpack_require__(95);
exports.PageList = PageList_1.default;
var PageLink_1 = __webpack_require__(96);
exports.PageLink = PageLink_1.default;
var Panel_1 = __webpack_require__(97);
exports.Panel = Panel_1.default;
var PanelHeading_1 = __webpack_require__(98);
exports.PanelHeading = PanelHeading_1.default;
var PanelTabs_1 = __webpack_require__(99);
exports.PanelTabs = PanelTabs_1.default;
var PanelTab_1 = __webpack_require__(100);
exports.PanelTab = PanelTab_1.default;
var PanelBlock_1 = __webpack_require__(101);
exports.PanelBlock = PanelBlock_1.default;
var PanelIcon_1 = __webpack_require__(102);
exports.PanelIcon = PanelIcon_1.default;
var Tabs_1 = __webpack_require__(103);
exports.Tabs = Tabs_1.default;
var Tab_1 = __webpack_require__(104);
exports.Tab = Tab_1.default;
var TabList_1 = __webpack_require__(105);
exports.TabList = TabList_1.default;
var TabLink_1 = __webpack_require__(106);
exports.TabLink = TabLink_1.default;
var Container_1 = __webpack_require__(107);
exports.Container = Container_1.default;
var Footer_1 = __webpack_require__(108);
exports.Footer = Footer_1.default;
var Section_1 = __webpack_require__(109);
exports.Section = Section_1.default;
var Hero_1 = __webpack_require__(110);
exports.Hero = Hero_1.default;
var HeroHeader_1 = __webpack_require__(111);
exports.HeroHeader = HeroHeader_1.default;
var HeroBody_1 = __webpack_require__(112);
exports.HeroBody = HeroBody_1.default;
var HeroVideo_1 = __webpack_require__(113);
exports.HeroVideo = HeroVideo_1.default;
var HeroFooter_1 = __webpack_require__(114);
exports.HeroFooter = HeroFooter_1.default;
var bulma_1 = __webpack_require__(1);
exports.withHelpersModifiers = bulma_1.withHelpersModifiers;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Columns(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('columns', {
        'is-centered': props.isCentered,
        'is-desktop': props.isDesktop,
        'is-gapless': props.isGapless,
        'is-grid': props.isGrid,
        'is-mobile': props.isMobile,
        'is-multiline': props.isMultiline,
        'is-vcentered': props.isVCentered,
    }, props.className);
    var isMobile = props.isMobile, isDesktop = props.isDesktop, isGapless = props.isGapless, isGrid = props.isGrid, isMultiline = props.isMultiline, isVCentered = props.isVCentered, isCentered = props.isCentered, HTMLProps = tslib_1.__rest(props, ["isMobile", "isDesktop", "isGapless", "isGrid", "isMultiline", "isVCentered", "isCentered"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Columns = Columns;
var HOC = bulma_1.withHelpersModifiers(Columns);
exports.default = HOC;


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var grid_1 = __webpack_require__(5);
function Column(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('column', tslib_1.__assign({}, helpers_1.combineModifiers(props, grid_1.getSizeModifiers, grid_1.getOffsetModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, grid_1.removeSizeProps, grid_1.removeOffsetProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Column = Column;
var HOC = bulma_1.withHelpersModifiers(Column);
exports.default = HOC;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var grid_1 = __webpack_require__(5);
function Tile(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('tile', tslib_1.__assign({ 'is-ancestor': props.isAncestor, 'is-child': props.isChild, 'is-parent': props.isParent, 'is-vertical': props.isVertical }, grid_1.getGridSizesModifiers(props.isSize)), props.className);
    var isAncestor = props.isAncestor, isChild = props.isChild, isParent = props.isParent, isVertical = props.isVertical, rest = tslib_1.__rest(props, ["isAncestor", "isChild", "isParent", "isVertical"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, grid_1.removeGridSizesProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tile = Tile;
var HOC = bulma_1.withHelpersModifiers(Tile);
exports.default = HOC;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Box(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('box', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Box = Box;
var HOC = bulma_1.withHelpersModifiers(Box);
exports.default = HOC;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Button(props) {
    var className = classNames('button', tslib_1.__assign({ 'is-inverted': props.isInverted, 'is-link': props.isLink, 'is-outlined': props.isOutlined, 'is-static': props.isStatic }, helpers_1.combineModifiers(props, bulma_1.getStateModifiers, bulma_1.getColorModifiers, bulma_1.getLoadingModifiers, bulma_1.getSizeModifiers)), props.className);
    var render = props.render, isLink = props.isLink, isOutlined = props.isOutlined, isInverted = props.isInverted, isStatic = props.isStatic, rest = tslib_1.__rest(props, ["render", "isLink", "isOutlined", "isInverted", "isStatic"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeStateProps, bulma_1.removeColorProps, bulma_1.removeLoadingProps, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    var anchor = (React.createElement("a", tslib_1.__assign({}, HTMLProps, { role: "button", className: className })));
    var button = (React.createElement("button", tslib_1.__assign({}, HTMLProps, { type: props.type || 'button', className: className })));
    return props.href ? anchor : button;
}
exports.Button = Button;
var HOC = bulma_1.withHelpersModifiers(Button);
exports.default = HOC;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Content(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('content', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Content = Content;
var HOC = bulma_1.withHelpersModifiers(Content);
exports.default = HOC;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Delete(props) {
    var className = classNames('delete', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var render = props.render, rest = tslib_1.__rest(props, ["render"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    var anchor = (React.createElement("a", tslib_1.__assign({ role: "button" }, HTMLProps, { className: className })));
    var button = (React.createElement("button", tslib_1.__assign({ type: props.type || 'button' }, HTMLProps, { className: className })));
    return props.href ? anchor : button;
}
exports.Delete = Delete;
var HOC = bulma_1.withHelpersModifiers(Delete);
exports.default = HOC;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var isAlignOption = helpers_1.isOption(bulma_1.isLeft, bulma_1.isRight);
function Icon(_a) {
    var _b;
    var children = _a.children, props = tslib_1.__rest(_a, ["children"]);
    var className = classNames('icon', tslib_1.__assign({}, (isAlignOption(props.isAlign) ? (_b = {}, _b["is-" + props.isAlign] = true, _b) : {}), bulma_1.getSizeModifiers(props)));
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    var icon = (React.createElement("span", tslib_1.__assign({}, HTMLProps, { className: className }),
        React.createElement("i", { className: "" + props.className, "aria-hidden": "true" })));
    return icon;
}
exports.Icon = Icon;
var HOC = bulma_1.withHelpersModifiers(Icon);
exports.default = HOC;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var isRatio = helpers_1.is({
    '16:9': '16by9',
    '1:1': '1by1',
    '2:1': '2by1',
    '3:2': '3by2',
    '4:3': '4by3',
    'square': 'square',
});
var isSize = helpers_1.is({
    '128x128': true,
    '16x16': true,
    '24x24': true,
    '32x32': true,
    '48x48': true,
    '64x64': true,
    '96x96': true,
});
function getSizeModifiers(_a) {
    var size = _a.isSize;
    var _b;
    return tslib_1.__assign({}, (isSize(size) ? (_b = {}, _b["is-" + size] = true, _b) : {}));
}
function getRatioModifiers(_a) {
    var ratio = _a.isRatio;
    var _b;
    return tslib_1.__assign({}, (isRatio(ratio) ? (_b = {}, _b["is-" + isRatio(ratio)] = true, _b) : {}));
}
function removeImageProps(props) {
    var isSize = props.isSize, isRatio = props.isRatio, rest = tslib_1.__rest(props, ["isSize", "isRatio"]);
    return rest;
}
function Image(props) {
    var className = classNames('image', tslib_1.__assign({}, helpers_1.combineModifiers(props, getSizeModifiers, getRatioModifiers)), props.className);
    var _a = helpers_1.getHTMLProps(props, removeImageProps), children = _a.children, src = _a.src, HTMLProps = tslib_1.__rest(_a, ["children", "src"]);
    return (React.createElement("figure", tslib_1.__assign({}, HTMLProps, { className: className }),
        React.createElement("img", { src: src })));
}
exports.Image = Image;
var HOC = bulma_1.withHelpersModifiers(Image);
exports.default = HOC;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Notification(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('notification', tslib_1.__assign({}, bulma_1.getColorModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Notification = Notification;
var HOC = bulma_1.withHelpersModifiers(Notification);
exports.default = HOC;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Progress(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'progress' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('progress', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getSizeModifiers, bulma_1.getColorModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Progress = Progress;
var HOC = bulma_1.withHelpersModifiers(Progress);
exports.default = HOC;


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Table(props) {
    var className = classNames('table', {
        'is-bordered': props.isBordered,
        'is-narrow': props.isNarrow,
        'is-striped': props.isStriped,
    }, props.className);
    var isBordered = props.isBordered, isStriped = props.isStriped, isNarrow = props.isNarrow, HTMLProps = tslib_1.__rest(props, ["isBordered", "isStriped", "isNarrow"]);
    return (React.createElement("table", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.Table = Table;
var HOC = bulma_1.withHelpersModifiers(Table);
exports.default = HOC;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Tag(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'span' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('tag', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tag = Tag;
var HOC = bulma_1.withHelpersModifiers(Tag);
exports.default = HOC;


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Title(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'h1' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('title', tslib_1.__assign({}, bulma_1.getHeadingModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeHeadingProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Title = Title;
var HOC = bulma_1.withHelpersModifiers(Title);
exports.default = HOC;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Subtitle(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'h2' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('subtitle', tslib_1.__assign({}, bulma_1.getHeadingModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeHeadingProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Subtitle = Subtitle;
var HOC = bulma_1.withHelpersModifiers(Subtitle);
exports.default = HOC;


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Heading(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('heading', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Heading = Heading;
var HOC = bulma_1.withHelpersModifiers(Heading);
exports.default = HOC;


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Checkbox(props) {
    var wrapperClassName = classNames('checkbox', props.className);
    var children = props.children, className = props.className, HTMLProps = tslib_1.__rest(props, ["children", "className"]);
    return (React.createElement("label", { className: wrapperClassName },
        React.createElement("input", tslib_1.__assign({}, HTMLProps, { type: "checkbox" })),
        children));
}
exports.Checkbox = Checkbox;
var HOC = bulma_1.withHelpersModifiers(Checkbox);
exports.default = HOC;


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var isDirection = helpers_1.isOption(bulma_1.isLeft, bulma_1.isRight);
var getModifier = function (modifier) {
    var _a;
    if (modifier === true) {
        return { 'has-icons-left has-icons-right': true };
    }
    else if (typeof modifier === 'string') {
        return isDirection(modifier) ? (_a = {}, _a["has-icons-" + modifier] = true, _a) : {};
    }
    else if (Array.isArray(modifier)) {
        return modifier.map(function (str) { return str.toLowerCase().trim(); })
            .reduce(function (init, option) {
            var _a;
            return isDirection(option) ? tslib_1.__assign({}, init, (_a = {}, _a["has-icons-" + option] = true, _a)) : init;
        }, {});
    }
    return {};
};
function Control(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('control', tslib_1.__assign({}, getModifier(props.hasIcons), { 'is-expanded': props.isExpanded }, bulma_1.getLoadingModifiers(props)), props.className);
    var hasIcons = props.hasIcons, isExpanded = props.isExpanded, rest = tslib_1.__rest(props, ["hasIcons", "isExpanded"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeLoadingProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Control = Control;
var HOC = bulma_1.withHelpersModifiers(Control);
exports.default = HOC;


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Help(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('help', tslib_1.__assign({}, bulma_1.getColorModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Help = Help;
var HOC = bulma_1.withHelpersModifiers(Help);
exports.default = HOC;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Input(props) {
    var className = classNames('input', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers, bulma_1.getStateModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps, bulma_1.removeStateProps);
    return React.createElement("input", tslib_1.__assign({}, HTMLProps, { className: className, type: props.type || 'text' }));
}
exports.Input = Input;
var HOC = bulma_1.withHelpersModifiers(Input);
exports.default = HOC;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Label(props) {
    var className = classNames('label', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    return (React.createElement("label", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.Label = Label;
var HOC = bulma_1.withHelpersModifiers(Label);
exports.default = HOC;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Radio(props) {
    var wrapperClassName = classNames('radio', props.className);
    var children = props.children, className = props.className, HTMLProps = tslib_1.__rest(props, ["children", "className"]);
    return (React.createElement("label", { className: wrapperClassName },
        React.createElement("input", tslib_1.__assign({}, HTMLProps, { type: "radio" })),
        children));
}
exports.Radio = Radio;
var HOC = bulma_1.withHelpersModifiers(Radio);
exports.default = HOC;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Select(props) {
    var wrapperClassName = classNames('select', tslib_1.__assign({ 'is-disabled': props.disabled }, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers, bulma_1.getLoadingModifiers)), props.className);
    var _a = helpers_1.getHTMLProps(props, bulma_1.removeColorProps, bulma_1.removeSizeProps, bulma_1.removeLoadingProps), children = _a.children, className = _a.className, HTMLProps = tslib_1.__rest(_a, ["children", "className"]);
    return (React.createElement("div", { className: wrapperClassName },
        React.createElement("select", tslib_1.__assign({}, HTMLProps), children)));
}
exports.Select = Select;
var HOC = bulma_1.withHelpersModifiers(Select);
exports.default = HOC;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function TextArea(props) {
    var className = classNames('textarea', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getSizeModifiers, bulma_1.getStateModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps, bulma_1.removeStateProps);
    return (React.createElement("textarea", tslib_1.__assign({}, HTMLProps, { className: className })));
}
exports.TextArea = TextArea;
var HOC = bulma_1.withHelpersModifiers(TextArea);
exports.default = HOC;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var getModifier = function (modifier, helper, isDirection) {
    var _a, _b;
    if (modifier === true) {
        return _a = {}, _a["" + helper] = true, _a;
    }
    else if (typeof modifier === 'string') {
        return isDirection(modifier) ? (_b = {}, _b[helper + " " + helper + "-" + modifier] = true, _b) : {};
    }
    return {};
};
function Field(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field', tslib_1.__assign({}, getModifier(props.isGrouped, 'is-grouped', helpers_1.isOption(bulma_1.isRight, bulma_1.isCentered)), getModifier(props.hasAddons, 'has-addons', helpers_1.isOption(bulma_1.isRight, bulma_1.isCentered, bulma_1.isFullWidth)), { 'is-horizontal': props.isHorizontal }), props.className);
    var isGrouped = props.isGrouped, hasAddons = props.hasAddons, isHorizontal = props.isHorizontal, HTMLProps = tslib_1.__rest(props, ["isGrouped", "hasAddons", "isHorizontal"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Field = Field;
var HOC = bulma_1.withHelpersModifiers(Field);
exports.default = HOC;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function FieldBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.FieldBody = FieldBody;
var HOC = bulma_1.withHelpersModifiers(FieldBody);
exports.default = HOC;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function FieldLabel(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('field-label', tslib_1.__assign({ 'is-normal': props.isNormal }, bulma_1.getSizeModifiers(props)), props.className);
    var isNormal = props.isNormal, rest = tslib_1.__rest(props, ["isNormal"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.FieldLabel = FieldLabel;
var HOC = bulma_1.withHelpersModifiers(FieldLabel);
exports.default = HOC;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var separatorClassname = function (separator) {
    var _a;
    return separator ? (_a = {}, _a["has-" + separator + "-separator"] = true, _a) : {};
};
function Breadcrumb(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, hasSeparator = _a.hasSeparator, props = tslib_1.__rest(_a, ["tag", "hasSeparator"]);
    var className = classNames('breadcrumb', tslib_1.__assign({}, separatorClassname(hasSeparator), helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Breadcrumb = Breadcrumb;
var HOC = bulma_1.withHelpersModifiers(Breadcrumb);
exports.default = HOC;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function BreadcrumbItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'li' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames(tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.BreadcrumbItem = BreadcrumbItem;
var HOC = bulma_1.withHelpersModifiers(BreadcrumbItem);
exports.default = HOC;


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Card(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Card = Card;
var HOC = bulma_1.withHelpersModifiers(Card);
exports.default = HOC;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function CardImage(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-image', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardImage = CardImage;
var HOC = bulma_1.withHelpersModifiers(CardImage);
exports.default = HOC;


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function CardContent(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-content', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardContent = CardContent;
var HOC = bulma_1.withHelpersModifiers(CardContent);
exports.default = HOC;


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var classNames = __webpack_require__(3);
var bulma_1 = __webpack_require__(1);
function CardHeader(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'header' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-header', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardHeader = CardHeader;
var HOC = bulma_1.withHelpersModifiers(CardHeader);
exports.default = HOC;


/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var classNames = __webpack_require__(3);
var bulma_1 = __webpack_require__(1);
function CardHeaderTitle(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-header-title', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardHeaderTitle = CardHeaderTitle;
var HOC = bulma_1.withHelpersModifiers(CardHeaderTitle);
exports.default = HOC;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var classNames = __webpack_require__(3);
var bulma_1 = __webpack_require__(1);
function CardHeaderIcon(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('card-header-icon', props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardHeaderIcon = CardHeaderIcon;
var HOC = bulma_1.withHelpersModifiers(CardHeaderIcon);
exports.default = HOC;


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function CardFooter(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('card-footer', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.CardFooter = CardFooter;
var HOC = bulma_1.withHelpersModifiers(CardFooter);
exports.default = HOC;


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function CardFooterItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('card-footer-item', props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, props, { className: className }));
}
exports.CardFooterItem = CardFooterItem;
var HOC = bulma_1.withHelpersModifiers(CardFooterItem);
exports.default = HOC;


/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Dropdown(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, isHoverable = _a.isHoverable, props = tslib_1.__rest(_a, ["tag", "isHoverable"]);
    var className = classNames('dropdown', tslib_1.__assign({ 'is-hoverable': isHoverable }, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getAlignmentModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers, bulma_1.removeAlignmentProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Dropdown = Dropdown;
var HOC = bulma_1.withHelpersModifiers(Dropdown);
exports.default = HOC;


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function DropdownContent(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('dropdown-content', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.DropdownContent = DropdownContent;
var HOC = bulma_1.withHelpersModifiers(DropdownContent);
exports.default = HOC;


/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function DropdownDivider(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'hr' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('dropdown-divider', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.DropdownDivider = DropdownDivider;
var HOC = bulma_1.withHelpersModifiers(DropdownDivider);
exports.default = HOC;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function DropdownItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('dropdown-item', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.DropdownItem = DropdownItem;
var HOC = bulma_1.withHelpersModifiers(DropdownItem);
exports.default = HOC;


/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function DropdownMenu(_a) {
    var props = tslib_1.__rest(_a, []);
    var className = classNames('dropdown-menu', props.className);
    return React.createElement('div', tslib_1.__assign({}, props, { className: className }));
}
exports.DropdownMenu = DropdownMenu;
var HOC = bulma_1.withHelpersModifiers(DropdownMenu);
exports.default = HOC;


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function DropdownTrigger(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('dropdown-trigger', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.DropdownTrigger = DropdownTrigger;
var HOC = bulma_1.withHelpersModifiers(DropdownTrigger);
exports.default = HOC;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Level(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level', {
        'is-mobile': props.isMobile,
    }, props.className);
    var isMobile = props.isMobile, HTMLProps = tslib_1.__rest(props, ["isMobile"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Level = Level;
var HOC = bulma_1.withHelpersModifiers(Level);
exports.default = HOC;


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function LevelItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level-item', {
        'is-flexible': props.isFlexible,
    }, props.className);
    var isFlexible = props.isFlexible, HTMLProps = tslib_1.__rest(props, ["isFlexible"]);
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.LevelItem = LevelItem;
var HOC = bulma_1.withHelpersModifiers(LevelItem);
exports.default = HOC;


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function LevelLeft(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level-left', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.LevelLeft = LevelLeft;
var HOC = bulma_1.withHelpersModifiers(LevelLeft);
exports.default = HOC;


/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function LevelRight(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('level-right', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.LevelRight = LevelRight;
var HOC = bulma_1.withHelpersModifiers(LevelRight);
exports.default = HOC;


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Media(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'article' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('media', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Media = Media;
var HOC = bulma_1.withHelpersModifiers(Media);
exports.default = HOC;


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MediaContent(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('media-content', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MediaContent = MediaContent;
var HOC = bulma_1.withHelpersModifiers(MediaContent);
exports.default = HOC;


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MediaLeft(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('media-left', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MediaLeft = MediaLeft;
var HOC = bulma_1.withHelpersModifiers(MediaLeft);
exports.default = HOC;


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MediaRight(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('media-right', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MediaRight = MediaRight;
var HOC = bulma_1.withHelpersModifiers(MediaRight);
exports.default = HOC;


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Menu(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'aside' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('menu', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Menu = Menu;
var HOC = bulma_1.withHelpersModifiers(Menu);
exports.default = HOC;


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MenuLabel(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('menu-label', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MenuLabel = MenuLabel;
var HOC = bulma_1.withHelpersModifiers(MenuLabel);
exports.default = HOC;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MenuList(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'ul' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('menu-list', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MenuList = MenuList;
var HOC = bulma_1.withHelpersModifiers(MenuList);
exports.default = HOC;


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function MenuLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames(tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.MenuLink = MenuLink;
var HOC = bulma_1.withHelpersModifiers(MenuLink);
exports.default = HOC;


/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Message(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'article' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('message', tslib_1.__assign({}, bulma_1.getColorModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeColorProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Message = Message;
var HOC = bulma_1.withHelpersModifiers(Message);
exports.default = HOC;


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MessageHeader(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('message-header', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MessageHeader = MessageHeader;
var HOC = bulma_1.withHelpersModifiers(MessageHeader);
exports.default = HOC;


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function MessageBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('message-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.MessageBody = MessageBody;
var HOC = bulma_1.withHelpersModifiers(MessageBody);
exports.default = HOC;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Modal(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Modal = Modal;
var HOC = bulma_1.withHelpersModifiers(Modal);
exports.default = HOC;


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalBackground(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-background', props.className);
    var children = props.children, HTMLProps = tslib_1.__rest(props, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalBackground = ModalBackground;
var HOC = bulma_1.withHelpersModifiers(ModalBackground);
exports.default = HOC;


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalContent(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-content', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalContent = ModalContent;
var HOC = bulma_1.withHelpersModifiers(ModalContent);
exports.default = HOC;


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function ModalClose(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'button' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('modal-close', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.ModalClose = ModalClose;
var HOC = bulma_1.withHelpersModifiers(ModalClose);
exports.default = HOC;


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalCard(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCard = ModalCard;
var HOC = bulma_1.withHelpersModifiers(ModalCard);
exports.default = HOC;


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalCardHeader(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'header' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card-head', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCardHeader = ModalCardHeader;
var HOC = bulma_1.withHelpersModifiers(ModalCardHeader);
exports.default = HOC;


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalCardTitle(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'h1' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card-title', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCardTitle = ModalCardTitle;
var HOC = bulma_1.withHelpersModifiers(ModalCardTitle);
exports.default = HOC;


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalCardBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'section' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCardBody = ModalCardBody;
var HOC = bulma_1.withHelpersModifiers(ModalCardBody);
exports.default = HOC;


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function ModalCardFooter(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('modal-card-foot', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.ModalCardFooter = ModalCardFooter;
var HOC = bulma_1.withHelpersModifiers(ModalCardFooter);
exports.default = HOC;


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Nav(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav', {
        'has-shadow': props.hasShadow,
    }, props.className);
    var hasShadow = props.hasShadow, HTMLProps = tslib_1.__rest(props, ["hasShadow"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Nav = Nav;
var HOC = bulma_1.withHelpersModifiers(Nav);
exports.default = HOC;


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavLeft(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav-left', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavLeft = NavLeft;
var HOC = bulma_1.withHelpersModifiers(NavLeft);
exports.default = HOC;


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavCenter(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav-center', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavCenter = NavCenter;
var HOC = bulma_1.withHelpersModifiers(NavCenter);
exports.default = HOC;


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavRight(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav-right', tslib_1.__assign({ 'nav-menu': props.isMenu }, bulma_1.getActiveModifiers(props)), props.className);
    var isMenu = props.isMenu, rest = tslib_1.__rest(props, ["isMenu"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavRight = NavRight;
var HOC = bulma_1.withHelpersModifiers(NavRight);
exports.default = HOC;


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavToggle(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'span' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('nav-toggle', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var _c = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers), children = _c.children, HTMLProps = tslib_1.__rest(_c, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }), React.createElement('span', null), React.createElement('span', null), React.createElement('span', null));
}
exports.NavToggle = NavToggle;
var HOC = bulma_1.withHelpersModifiers(NavToggle);
exports.default = HOC;


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('nav-item', tslib_1.__assign({ 'is-brand': props.isBrand, 'is-tab': props.isTab }, bulma_1.getActiveModifiers(props)), props.className);
    var isTab = props.isTab, isBrand = props.isBrand, rest = tslib_1.__rest(props, ["isTab", "isBrand"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavItem = NavItem;
var HOC = bulma_1.withHelpersModifiers(NavItem);
exports.default = HOC;


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Navbar(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, isTransparent = _a.isTransparent, props = tslib_1.__rest(_a, ["tag", "isTransparent"]);
    var className = classNames('navbar', {
        'is-transparent': isTransparent,
    }, props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Navbar = Navbar;
var HOC = bulma_1.withHelpersModifiers(Navbar);
exports.default = HOC;


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavbarBrand(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-brand', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarBrand = NavbarBrand;
var HOC = bulma_1.withHelpersModifiers(NavbarBrand);
exports.default = HOC;


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavbarBurger(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-burger', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var _c = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers), children = _c.children, HTMLProps = tslib_1.__rest(_c, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }), React.createElement('span', null), React.createElement('span', null), React.createElement('span', null));
}
exports.NavbarBurger = NavbarBurger;
var HOC = bulma_1.withHelpersModifiers(NavbarBurger);
exports.default = HOC;


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavbarMenu(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-menu', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavbarMenu = NavbarMenu;
var HOC = bulma_1.withHelpersModifiers(NavbarMenu);
exports.default = HOC;


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavbarStart(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-start', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarStart = NavbarStart;
var HOC = bulma_1.withHelpersModifiers(NavbarStart);
exports.default = HOC;


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavbarEnd(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('navbar-end', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarEnd = NavbarEnd;
var HOC = bulma_1.withHelpersModifiers(NavbarEnd);
exports.default = HOC;


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavbarItem(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, isHoverable = _a.isHoverable, hasDropdown = _a.hasDropdown, props = tslib_1.__rest(_a, ["tag", "render", "isHoverable", "hasDropdown"]);
    var className = classNames('navbar-item', tslib_1.__assign({ 'has-dropdown': hasDropdown, 'is-hoverable': isHoverable }, bulma_1.getActiveModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavbarItem = NavbarItem;
var HOC = bulma_1.withHelpersModifiers(NavbarItem);
exports.default = HOC;


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function NavbarLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('navbar-link', tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className);
    if (render)
        return render(tslib_1.__assign({}, props, { className: className }));
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.NavbarLink = NavbarLink;
var HOC = bulma_1.withHelpersModifiers(NavbarLink);
exports.default = HOC;


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavbarDropdown(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, isBoxed = _a.isBoxed, props = tslib_1.__rest(_a, ["tag", "isBoxed"]);
    var className = classNames('navbar-dropdown', {
        'is-boxed': isBoxed,
    }, props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarDropdown = NavbarDropdown;
var HOC = bulma_1.withHelpersModifiers(NavbarDropdown);
exports.default = HOC;


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function NavbarDivider(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'hr' : _b, isBoxed = _a.isBoxed, props = tslib_1.__rest(_a, ["tag", "isBoxed"]);
    var className = classNames('navbar-divider', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.NavbarDivider = NavbarDivider;
var HOC = bulma_1.withHelpersModifiers(NavbarDivider);
exports.default = HOC;


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Pagination(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('pagination', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Pagination = Pagination;
var HOC = bulma_1.withHelpersModifiers(Pagination);
exports.default = HOC;


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function PageControl(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames(tslib_1.__assign({ 'pagination-next': !props.isPrevious && props.isNext, 'pagination-previous': !props.isNext }, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getFocusedModifiers)), props.className);
    var isNext = props.isNext, isPrevious = props.isPrevious, rest = tslib_1.__rest(props, ["isNext", "isPrevious"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers, bulma_1.removeFocusedModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.PageControl = PageControl;
var HOC = bulma_1.withHelpersModifiers(PageControl);
exports.default = HOC;


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Ellipsis(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'span' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('pagination-ellipsis', tslib_1.__assign({}, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getFocusedModifiers)), props.className);
    var _c = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers, bulma_1.removeFocusedModifiers), children = _c.children, HTMLProps = tslib_1.__rest(_c, ["children"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }), '\u2026');
}
exports.Ellipsis = Ellipsis;
var HOC = bulma_1.withHelpersModifiers(Ellipsis);
exports.default = HOC;


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Page(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'li' : _b, props = tslib_1.__rest(_a, ["tag"]);
    return React.createElement(tag, props);
}
exports.Page = Page;
var HOC = bulma_1.withHelpersModifiers(Page);
exports.default = HOC;


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function PageList(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'ul' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('pagination-list', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.PageList = PageList;
var HOC = bulma_1.withHelpersModifiers(PageList);
exports.default = HOC;


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function PageLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('pagination-link', tslib_1.__assign({ 'is-current': props.isCurrent }, helpers_1.combineModifiers(props, bulma_1.getActiveModifiers, bulma_1.getFocusedModifiers)), props.className);
    var isCurrent = props.isCurrent, rest = tslib_1.__rest(props, ["isCurrent"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers, bulma_1.removeFocusedModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.PageLink = PageLink;
var HOC = bulma_1.withHelpersModifiers(PageLink);
exports.default = HOC;


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Panel(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'nav' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('panel', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Panel = Panel;
var HOC = bulma_1.withHelpersModifiers(Panel);
exports.default = HOC;


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function PanelHeading(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'p' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('panel-heading', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.PanelHeading = PanelHeading;
var HOC = bulma_1.withHelpersModifiers(PanelHeading);
exports.default = HOC;


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function PanelTabs(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('panel-tabs', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.PanelTabs = PanelTabs;
var HOC = bulma_1.withHelpersModifiers(PanelTabs);
exports.default = HOC;


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function PanelTab(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames(tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.PanelTab = PanelTab;
var HOC = bulma_1.withHelpersModifiers(PanelTab);
exports.default = HOC;


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function PanelBlock(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    var className = classNames('panel-block', tslib_1.__assign({ 'is-wrapped': props.isWrapped }, bulma_1.getActiveModifiers(props)), props.className);
    var isWrapped = props.isWrapped, rest = tslib_1.__rest(props, ["isWrapped"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeActiveModifiers);
    if (render)
        return render(tslib_1.__assign({}, HTMLProps, { className: className }));
    return React.createElement((props.href ? 'a' : tag), tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.PanelBlock = PanelBlock;
var HOC = bulma_1.withHelpersModifiers(PanelBlock);
exports.default = HOC;


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function PanelIcon(_a) {
    var children = _a.children, props = tslib_1.__rest(_a, ["children"]);
    var icon = (React.createElement("span", tslib_1.__assign({}, props, { className: "panel-icon" }),
        React.createElement("i", { className: "" + props.className, "aria-hidden": "true" })));
    return icon;
}
exports.PanelIcon = PanelIcon;
var HOC = bulma_1.withHelpersModifiers(PanelIcon);
exports.default = HOC;


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Tabs(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('tabs', tslib_1.__assign({ 'is-boxed': props.isBoxed, 'is-toggle': props.isToggle }, helpers_1.combineModifiers(props, bulma_1.getAlignmentModifiers, bulma_1.getSizeModifiers)), props.className);
    var isBoxed = props.isBoxed, isToggle = props.isToggle, rest = tslib_1.__rest(props, ["isBoxed", "isToggle"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeAlignmentProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tabs = Tabs;
var HOC = bulma_1.withHelpersModifiers(Tabs);
exports.default = HOC;


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Tab(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'li' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames(tslib_1.__assign({}, bulma_1.getActiveModifiers(props)), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeActiveModifiers);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Tab = Tab;
var HOC = bulma_1.withHelpersModifiers(Tab);
exports.default = HOC;


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
var isAlignOption = helpers_1.isOption(bulma_1.isLeft, bulma_1.isCenter, bulma_1.isRight);
function TabList(_a) {
    var _b;
    var _c = _a.tag, tag = _c === void 0 ? 'ul' : _c, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames(tslib_1.__assign({}, (isAlignOption(props.isAlign) ? (_b = {}, _b["is-" + props.isAlign] = true, _b) : {})), props.className) || undefined;
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeAlignmentProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.TabList = TabList;
var HOC = bulma_1.withHelpersModifiers(TabList);
exports.default = HOC;


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function TabLink(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'a' : _b, render = _a.render, props = tslib_1.__rest(_a, ["tag", "render"]);
    if (render)
        return render(tslib_1.__assign({}, props));
    return React.createElement(tag, props);
}
exports.TabLink = TabLink;
var HOC = bulma_1.withHelpersModifiers(TabLink);
exports.default = HOC;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Container(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('container', {
        'is-fluid': props.isFluid,
    }, props.className);
    var isFluid = props.isFluid, HTMLProps = tslib_1.__rest(props, ["isFluid"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Container = Container;
var HOC = bulma_1.withHelpersModifiers(Container);
exports.default = HOC;


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function Footer(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('footer', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.Footer = Footer;
var HOC = bulma_1.withHelpersModifiers(Footer);
exports.default = HOC;


/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Section(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'section' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('section', tslib_1.__assign({}, bulma_1.getSizeModifiers(props)), props.className);
    var HTMLProps = helpers_1.getHTMLProps(props, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Section = Section;
var HOC = bulma_1.withHelpersModifiers(Section);
exports.default = HOC;


/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
var helpers_1 = __webpack_require__(4);
function Hero(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'section' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero', tslib_1.__assign({ 'is-bold': props.isBold, 'is-fullheight': props.isFullHeight, 'is-halfheight': props.isHalfHeight }, helpers_1.combineModifiers(props, bulma_1.getColorModifiers, bulma_1.getSizeModifiers)), props.className);
    var isBold = props.isBold, isFullHeight = props.isFullHeight, rest = tslib_1.__rest(props, ["isBold", "isFullHeight"]);
    var HTMLProps = helpers_1.getHTMLProps(rest, bulma_1.removeColorProps, bulma_1.removeSizeProps);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.Hero = Hero;
var HOC = bulma_1.withHelpersModifiers(Hero);
exports.default = HOC;


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function HeroHeader(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'header' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero-head', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.HeroHeader = HeroHeader;
var HOC = bulma_1.withHelpersModifiers(HeroHeader);
exports.default = HOC;


/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function HeroBody(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero-body', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.HeroBody = HeroBody;
var HOC = bulma_1.withHelpersModifiers(HeroBody);
exports.default = HOC;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function HeroVideo(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'div' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero-video', {
        'is-transparent': props.isTransparent,
    }, props.className);
    var isTransparent = props.isTransparent, HTMLProps = tslib_1.__rest(props, ["isTransparent"]);
    return React.createElement(tag, tslib_1.__assign({}, HTMLProps, { className: className }));
}
exports.HeroVideo = HeroVideo;
var HOC = bulma_1.withHelpersModifiers(HeroVideo);
exports.default = HOC;


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = __webpack_require__(0);
var classNames = __webpack_require__(3);
var React = __webpack_require__(2);
var bulma_1 = __webpack_require__(1);
function HeroFooter(_a) {
    var _b = _a.tag, tag = _b === void 0 ? 'footer' : _b, props = tslib_1.__rest(_a, ["tag"]);
    var className = classNames('hero-foot', props.className);
    return React.createElement(tag, tslib_1.__assign({}, props, { className: className }));
}
exports.HeroFooter = HeroFooter;
var HOC = bulma_1.withHelpersModifiers(HeroFooter);
exports.default = HOC;


/***/ })
/******/ ]);
});
//# sourceMappingURL=bloomer.js.map